export interface Html {
  title: string;
  html: string;
}

export interface EditHtmlEvent {
  newRecord: boolean;
  data: Html;
}
