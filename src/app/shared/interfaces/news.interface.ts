import { FileDto } from './files.interface';
import { IRecord } from './record.interface';

export interface News extends IRecord {
  title: string;
  html: string;
  file: FileDto;
}
