export enum OrderDirection {
  Acs = 'asc',
  Desc = 'desc',
}

export interface TableData<T> {
  pageNumber: number;
  pageSize: number;
  totalElements: number;
  totalPages: number;
  elements: T[];
}

export interface SortStatus {
  orderBy?: string;
  orderDirection?: OrderDirection;
}

export type TableQueryParams = Partial<
  Pick<TableData<unknown>, 'pageNumber' | 'pageSize'>
> &
  SortStatus;
