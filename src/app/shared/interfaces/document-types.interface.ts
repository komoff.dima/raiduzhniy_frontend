import { IRecord } from './record.interface';

export interface DocumentTypeDto {
  name: string;
}

export type DocumentType = DocumentTypeDto & IRecord;
