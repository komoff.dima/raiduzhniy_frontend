export interface MinMax<Min = string, Max = string> {
  min?: Min;
  max?: Max;
}
