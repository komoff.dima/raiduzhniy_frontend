import { ImageCroppedEvent } from 'ngx-image-cropper';

export interface IImageCroppedEvent extends ImageCroppedEvent {
  fileName: string;
}

export interface FileDto {
  url: string;
  storagePath: string;
}
