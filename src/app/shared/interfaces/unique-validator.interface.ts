import { Observable } from 'rxjs';
import { DefaultResponse } from './response.interface';

export interface UniqueValidator {
  validateUnique(data: unknown): Observable<DefaultResponse>;
}
