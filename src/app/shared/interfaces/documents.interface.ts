import { DocumentType } from './document-types.interface';
import { FileDto } from './files.interface';
import { IRecord } from './record.interface';

export interface DocumentDto {
  name: string;
  description?: string;
  type: DocumentType;
  files: FileDto[];
  approvedAt: string;
}

export type IDocument = DocumentDto & IRecord;
