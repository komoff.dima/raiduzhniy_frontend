import { Content, Layer, Popup } from 'leaflet';

export interface MapMarkerDto {
  lat: number;
  long: number;
  caption: string;
}

export interface MapMarkerResponse extends MapMarkerDto {
  id: string;
}

export type IMarker = Omit<MapMarkerResponse, 'caption'> & {
  content: ((layer: Layer) => Content) | Content | Popup;
};
