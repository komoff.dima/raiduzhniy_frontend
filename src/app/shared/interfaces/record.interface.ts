export interface IRecord {
  id: string;
  createdAt?: string;
  editedAt?: string | null;
}
