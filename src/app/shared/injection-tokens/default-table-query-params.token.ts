import { InjectionToken } from '@angular/core';
import { TableQueryParams } from '../interfaces';

export const DEFAULT_QUERY_PARAMS_TOKEN = new InjectionToken<TableQueryParams>(
  'DEFAULT_QUERY_PARAMS'
);
