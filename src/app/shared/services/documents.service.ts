import { Injectable } from '@angular/core';
import { ApiService } from '@core/services';
import { Observable } from 'rxjs';
import { HttpBase } from '../abstract-classes';
import { AdaptedFilters } from '../components/filterable-content';
import { IDocument, TableData, TableQueryParams } from '../interfaces';

@Injectable()
export class DocumentsService extends HttpBase {
  constructor(protected apiService: ApiService) {
    super();
  }

  getDocuments(
    queryParams?: TableQueryParams,
    filters: AdaptedFilters = {}
  ): Observable<TableData<IDocument>> {
    return this.apiService.get(`documents`, {
      ...this.createHttpParams({ ...queryParams, filters }),
    });
  }
}
