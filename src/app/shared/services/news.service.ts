import { Injectable } from '@angular/core';
import { ApiService } from '@core/services';
import { News, TableData, TableQueryParams } from '@shared/interfaces';
import { Observable } from 'rxjs';
import { HttpBase } from '../abstract-classes';

@Injectable()
export class NewsService extends HttpBase {
  constructor(private apiService: ApiService) {
    super();
  }

  getNewsElements(queryParams?: TableQueryParams): Observable<TableData<News>> {
    return this.apiService.get(`news`, {
      ...this.createHttpParams(queryParams),
    });
  }

  deleteNews(newsId: string): Observable<{ success: boolean }> {
    return this.apiService.delete(`news/${newsId}/delete`);
  }

  createNews(formData: FormData): Observable<News> {
    return this.apiService.post(`news/create`, formData);
  }

  editNews(id: string, formData: FormData): Observable<News> {
    return this.apiService.put(`news/${id}/edit`, formData);
  }

  getNews(id: string): Observable<News> {
    return this.apiService.get(`news/${id}`);
  }
}
