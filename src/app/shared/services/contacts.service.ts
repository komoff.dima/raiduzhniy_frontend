import { Injectable } from '@angular/core';
import { ApiService } from '@core/services';
import { HtmlsServiceBase } from '@shared/abstract-classes';
import { Observable } from 'rxjs';
import { TableData } from '../interfaces';
import { MapMarkerDto, MapMarkerResponse } from '../interfaces';

@Injectable()
export class ContactsService extends HtmlsServiceBase {
  protected readonly url: string = 'contacts';

  constructor(apiService: ApiService) {
    super(apiService);
  }

  getMapsMarkers(): Observable<TableData<MapMarkerResponse>> {
    return this.apiService.get('contacts/markers');
  }

  deleteMapMarker(markerId: string): Observable<{ success: boolean }> {
    return this.apiService.delete(`contacts/markers/${markerId}/delete`);
  }

  addMapMarker(markerDto: MapMarkerDto): Observable<MapMarkerResponse> {
    return this.apiService.post('contacts/markers', markerDto);
  }
}
