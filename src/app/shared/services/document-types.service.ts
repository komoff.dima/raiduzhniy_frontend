import { Injectable } from '@angular/core';
import { ApiService } from '@core/services';
import { Observable } from 'rxjs';
import { HttpBase } from '../abstract-classes';
import { DocumentType, TableData, TableQueryParams } from '../interfaces';

@Injectable()
export class DocumentTypesService extends HttpBase {
  constructor(protected apiService: ApiService) {
    super();
  }

  getDocumentTypes(
    queryParams?: TableQueryParams
  ): Observable<TableData<DocumentType>> {
    return this.apiService.get('document-types', {
      ...this.createHttpParams(queryParams),
    });
  }
}
