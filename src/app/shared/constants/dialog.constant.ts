import { MatDialogConfig } from '@angular/material/dialog';

export const COMMON_DIALOG_CONFIG: MatDialogConfig = {
  maxWidth: '80vw',
  width: '400px',
};
