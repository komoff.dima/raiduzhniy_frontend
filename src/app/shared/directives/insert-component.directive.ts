import { ComponentType } from '@angular/cdk/overlay';
import {
  ComponentRef,
  Directive,
  Input,
  OnChanges,
  ViewContainerRef,
} from '@angular/core';

@Directive({
  selector: '[rdnInsertComponent]',
  standalone: true,
})
export class InsertComponentDirective<T> implements OnChanges {
  componentRef: ComponentRef<T>;

  @Input() rdnInsertComponent: ComponentType<T>;

  @Input() rdnInsertComponentData: object = {};

  @Input() rdnInsertComponentSharedData: unknown;

  constructor(private viewContainerRef: ViewContainerRef) {}

  ngOnChanges(): void {
    this.viewContainerRef.clear();
    this.componentRef = this.viewContainerRef.createComponent(
      this.rdnInsertComponent
    );
    Object.entries(this.rdnInsertComponentData).forEach(([key, value]) =>
      this.componentRef.setInput(key, value)
    );
  }
}
