import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';
import { PermissionsService } from '@core/services/permissions.service';
import { UntilDestroy } from '@ngneat/until-destroy';
import { Observable } from 'rxjs';
import { ConditionDependsVisibility } from '../abstract-classes';
import { UserRole } from '../enums';

@Directive({
  selector: '[rdnForRole]',
  standalone: true,
})
@UntilDestroy()
export class ForRolesDirective extends ConditionDependsVisibility {
  @Input() rdnForRole: UserRole[] = [];

  @Input() rdnForRoleAppearIfNoRoles = false;

  constructor(
    viewContainerRef: ViewContainerRef,
    templateRef: TemplateRef<any>,
    private permissionsService: PermissionsService
  ) {
    super(viewContainerRef, templateRef);
  }

  isVisible(): Observable<boolean> {
    return this.permissionsService.checkPermissions(
      this.rdnForRole,
      this.rdnForRoleAppearIfNoRoles
    );
  }
}
