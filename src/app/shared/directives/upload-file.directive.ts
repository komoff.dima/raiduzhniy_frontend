import {
  ComponentRef,
  Directive,
  ElementRef,
  EventEmitter,
  HostListener,
  Input,
  OnDestroy,
  OnInit,
  Output,
  Renderer2,
  ViewContainerRef,
} from '@angular/core';
import { FormControlName } from '@angular/forms';
import { MatIcon } from '@angular/material/icon';
import { MatInput } from '@angular/material/input';
import { UploadFileSelected } from '../components/upload-file';

@Directive({
  selector: 'input[matInput][formControlName][rdnUploadFile]',
  standalone: true,
})
export class UploadFileDirective implements OnInit, OnDestroy {
  listeners: (() => void)[] = [];

  fileInput: HTMLInputElement = this.renderer2.createElement('input');

  buttonsContainer: HTMLDivElement = this.renderer2.createElement('div');

  cancelIcon: ComponentRef<MatIcon>;

  uploadIcon: ComponentRef<MatIcon>;

  @Input() accept: string[];

  @Input() multiple = false;

  @Input() maxFileSize: number = 1e7;

  @Output() readonly selected = new EventEmitter<UploadFileSelected>();

  @HostListener('click') inputClicked() {
    this.fileInput.click();
  }

  constructor(
    private matInput: MatInput,
    private viewContainerRef: ViewContainerRef,
    private renderer2: Renderer2,
    private formControl: FormControlName,
    private elementRef: ElementRef<HTMLInputElement>
  ) {}

  ngOnInit() {
    this.adjustFileInput();
    this.generateIcons();
    this.groupIcons();
    this.setupListeners();
  }

  ngOnDestroy() {
    this.listeners.forEach(listener => listener());
    this.listeners = null;
  }

  private adjustFileInput(): void {
    setTimeout(() => (this.elementRef.nativeElement.readOnly = true), 0);

    this.renderer2.setAttribute(this.fileInput, 'type', 'file');

    if (this.multiple) {
      this.renderer2.setAttribute(this.fileInput, 'multiple', 'true');
    }

    if (this.accept) {
      this.renderer2.setAttribute(
        this.fileInput,
        'accept',
        this.accept.join(', ')
      );
    }
  }

  private generateIcons(): void {
    this.cancelIcon = this.viewContainerRef.createComponent(MatIcon);
    this.cancelIcon.instance.fontIcon = 'close';

    this.uploadIcon = this.viewContainerRef.createComponent(MatIcon);
    this.uploadIcon.instance.fontIcon = 'upload';
  }

  private filesChanged(event: any) {
    const { files } = event.target;
    const filesArray = (Array.from(files || []) as File[]).filter(
      file => file.size <= this.maxFileSize
    );
    if (filesArray.length) {
      this.formControl.control.setValue(filesArray);
      this.matInput.value = filesArray.map(file => file.name).join('; ');
      this.selected.emit({ originalEvent: event, files: filesArray });
      this.insertCancelButton();
    }
  }

  private canceled(): void {
    this.formControl.control.setValue(null);
    this.fileInput.value = null;
    this.selected.emit({ files: [] });
    this.removeCancelButton();
  }

  private groupIcons(): void {
    this.renderer2.setAttribute(
      this.buttonsContainer,
      'style',
      'position: absolute;right: 0;top: 50%;transform: translateY(-50%);display: flex;gap:8px'
    );

    setTimeout(() => {
      const parentElement = this.renderer2.parentNode(
        this.viewContainerRef.element.nativeElement
      );

      this.renderer2.appendChild(parentElement, this.buttonsContainer);

      this.renderer2.appendChild(
        this.buttonsContainer,
        this.uploadIcon.location.nativeElement
      );

      this.renderer2.removeChild(
        parentElement,
        this.cancelIcon.location.nativeElement
      );

      // two icons by 24px and 8px gap between them
      this.renderer2.setStyle(
        parentElement,
        'padding-right',
        `${24 * 2 + 8}px`
      );
    }, 0);
  }

  private insertCancelButton(): void {
    this.renderer2.insertBefore(
      this.buttonsContainer,
      this.cancelIcon.location.nativeElement,
      this.uploadIcon.location.nativeElement
    );
  }

  private removeCancelButton(): void {
    this.renderer2.removeChild(
      this.buttonsContainer,
      this.cancelIcon.location.nativeElement
    );
  }

  private setupListeners(): void {
    [
      {
        element: this.fileInput,
        listener: 'change',
        callback: this.filesChanged,
      },
      {
        element: this.uploadIcon.location.nativeElement,
        listener: 'click',
        callback: this.inputClicked,
      },
      {
        element: this.cancelIcon.location.nativeElement,
        listener: 'click',
        callback: this.canceled,
      },
    ].forEach(({ element, listener, callback }) => {
      this.listeners.push(
        this.renderer2.listen(element, listener, callback.bind(this))
      );
    });
  }
}
