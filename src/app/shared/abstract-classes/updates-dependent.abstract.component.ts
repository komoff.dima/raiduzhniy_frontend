import {
  Component,
  Inject,
  inject,
  InjectionToken,
  OnInit,
} from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { UpdatesFacade } from '@state/updates';
import { map, take } from 'rxjs';

const TOKEN = new InjectionToken<string>('COLLECTION_NAME');

@Component({ template: '' })
@UntilDestroy()
export abstract class UpdatesDependentComponent implements OnInit {
  private updatesFacade = inject(UpdatesFacade);

  protected constructor(@Inject(TOKEN) private collectionName: string) {}

  abstract updateState(): void;

  ngOnInit() {
    this.updatesFacade.updates$
      .pipe(
        take(1),
        untilDestroyed(this),
        map(updates => {
          const needToUpdate = updates[this.collectionName];

          return needToUpdate === undefined ? true : needToUpdate;
        })
      )
      .subscribe(needToUpdate => {
        if (needToUpdate) {
          this.updateState();
          this.updatesFacade.dispatchRemoveUpdate(this.collectionName);
        }
      });
  }
}
