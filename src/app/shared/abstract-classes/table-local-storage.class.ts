import { Inject } from '@angular/core';
import { DEFAULT_QUERY_PARAMS_TOKEN } from '../injection-tokens';
import { TableQueryParams } from '../interfaces';

export abstract class TableLocalStorageClass {
  readonly pageSizeOptions = [10, 25, 50];

  private _queryParams: TableQueryParams;

  get queryParams(): TableQueryParams {
    return this._queryParams;
  }

  protected set queryParams(params: TableQueryParams) {
    localStorage.setItem(this.localStorageKey, JSON.stringify(params));
    this._queryParams = params;
  }

  protected constructor(
    @Inject(DEFAULT_QUERY_PARAMS_TOKEN)
    private defaultQueryParams: TableQueryParams,
    protected localStorageKey: string
  ) {
    this.queryParams =
      JSON.parse(localStorage.getItem(localStorageKey)) ||
      this.defaultQueryParams;
  }
}
