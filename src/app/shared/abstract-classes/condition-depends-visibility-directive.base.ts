import { OnInit, TemplateRef, ViewContainerRef } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Observable } from 'rxjs';

@UntilDestroy()
export abstract class ConditionDependsVisibility implements OnInit {
  abstract isVisible(): Observable<boolean> | boolean;
  protected constructor(
    protected viewContainerRef: ViewContainerRef,
    protected templateRef: TemplateRef<any>
  ) {
    this.clear();
  }

  clear(): void {
    this.viewContainerRef.clear();
  }

  insert(): void {
    this.viewContainerRef.createEmbeddedView(this.templateRef);
  }

  ngOnInit(): void {
    this.run();
  }

  private run(): void {
    const isVisible = this.isVisible();

    if (typeof isVisible === 'boolean') {
      return this.processContent(isVisible);
    }

    isVisible
      .pipe(untilDestroyed(this))
      .subscribe(isVisible => this.processContent(isVisible));
  }

  private processContent(isVisible: boolean): void {
    isVisible ? this.insert() : this.clear();
  }
}
