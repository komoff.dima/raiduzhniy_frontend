import { HttpParams } from '@angular/common/http';

type FromObject = {
  [param: string]: string | number | boolean;
};

export abstract class HttpBase {
  protected createHttpParams<T extends object>(
    params: T = {} as T
  ): { params: HttpParams } {
    const a = Object.entries(params).reduce((hey, [key, value]) => {
      return {
        ...hey,
        [key]: typeof value === 'object' ? JSON.stringify(value) : value,
      };
    }, {});

    return {
      params: new HttpParams({ fromObject: a as FromObject }),
    };
  }
}
