import { Component, Input, Pipe, PipeTransform } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatTooltipModule } from '@angular/material/tooltip';
import { FileDto } from '../../interfaces';
import { FILE_ICONS_MAP } from './files.constant';

@Pipe({
  name: 'getIcon',
  standalone: true,
})
class GetIconPipe implements PipeTransform {
  transform(fileStoragePath: string): string {
    const fileExtention = fileStoragePath.split('.').pop();

    return FILE_ICONS_MAP[fileExtention] || 'download';
  }
}

@Component({
  selector: 'rdn-files',
  standalone: true,
  imports: [
    CommonModule,
    MatButtonModule,
    MatIconModule,
    MatTooltipModule,
    GetIconPipe,
  ],
  template: `
    <a
      *ngFor="let file of files; trackBy: trackBy"
      mat-icon-button
      [href]="file.url"
      target="_blank"
      [matTooltip]="'Завантажити файл'">
      <mat-icon>{{ file.storagePath | getIcon }}</mat-icon>
    </a>
  `,
  styleUrls: ['./files.component.scss'],
})
export class FilesComponent {
  @Input() files: FileDto[] = [];

  trackBy(index: number, file: FileDto): string {
    return file.storagePath;
  }
}
