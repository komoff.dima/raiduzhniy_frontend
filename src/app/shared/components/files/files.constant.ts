import { Dictionary } from '../../interfaces';

export const FILE_ICONS_MAP: Dictionary = {
  docx: 'description',
  pdf: 'picture_as_pdf',
  jpg: 'image',
  png: 'image',
};
