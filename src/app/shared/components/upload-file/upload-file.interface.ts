export interface UploadFileSelected {
  originalEvent?: Event;
  files: File[];
}
