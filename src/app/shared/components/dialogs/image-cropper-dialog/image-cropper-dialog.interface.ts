export interface ImageCropperDialogData {
  imageChangedEvent: Event;
  aspectRatio?: number;
}
