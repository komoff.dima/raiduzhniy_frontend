import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LogoWithoutLabelIconComponent } from '../../svg-icons';

@Component({
  selector: 'rdn-loader',
  standalone: true,
  imports: [CommonModule, LogoWithoutLabelIconComponent],
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss'],
})
export class LoaderComponent {}
