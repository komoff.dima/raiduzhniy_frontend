import { CommonModule } from '@angular/common';
import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  Output,
  SimpleChanges,
  ViewEncapsulation,
} from '@angular/core';
import { MatCardModule } from '@angular/material/card';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import {
  FeatureGroup,
  featureGroup,
  latLng,
  LeafletEvent,
  LeafletMouseEvent,
  Map,
  MapOptions,
  marker,
  PopupEvent,
  tileLayer,
} from 'leaflet';
import { ReplaySubject } from 'rxjs';
import { LoaderDirective } from '../../directives';
import { IMarker } from '../../interfaces/map.interface';
import { WrapperComponent } from '../wrapper/wrapper.component';

@Component({
  selector: 'rdn-map',
  standalone: true,
  imports: [
    CommonModule,
    LeafletModule,
    WrapperComponent,
    MatCardModule,
    LoaderDirective,
  ],
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
@UntilDestroy()
export class MapComponent implements OnChanges, OnDestroy {
  private readonly _markers$ = new ReplaySubject<IMarker[]>(1);

  private markerGroup: FeatureGroup = featureGroup();

  readonly mapOptions: MapOptions = {
    layers: [
      tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        opacity: 0.7,
        maxZoom: 19,
        detectRetina: true,
      }),
    ],
    zoom: 13,
    center: latLng(50.14929125334483, 30.749517122805962), // Coordinates of the center of Ukrainka
    attributionControl: false,
  };

  @Input() markers: IMarker[];

  @Input() isLoading = false;

  @Output() readonly mapZoomed = new EventEmitter<number>();

  @Output() readonly mapClicked = new EventEmitter<LeafletMouseEvent>();

  @Output() readonly popupOpened = new EventEmitter<PopupEvent>();

  @Output() readonly popupClosed = new EventEmitter<PopupEvent>();

  public map: Map;
  public zoom: number;

  ngOnChanges(changes: SimpleChanges) {
    const { markers } = changes;
    if (markers) {
      this._markers$.next(markers.currentValue);
    }
  }

  onMapReady(map: Map) {
    this.map = map;
    this.zoom = map.getZoom();
    this.map.on('popupopen', e => this.popupOpened.emit(e));
    this.map.on('popupclose', e => this.popupClosed.emit(e));
    this.mapZoomed.emit(this.zoom);

    this.markerGroup.addTo(this.map);

    this._markers$
      .pipe(untilDestroyed(this))
      .subscribe(markers => this.addMarkers(markers));
  }

  onMapZoomEnd(e: LeafletEvent) {
    this.zoom = e.target.getZoom();
    this.mapZoomed.emit(this.zoom);
  }

  mapCLicked(event: LeafletMouseEvent) {
    this.mapClicked.emit(event);
  }

  ngOnDestroy() {
    this.map.off();
    this.map = null;
  }

  private addMarkers(markers: IMarker[]) {
    this.markerGroup.clearLayers();

    (markers || []).forEach(({ lat, long, content }) => {
      const newMarker = marker([lat, long]).bindPopup(content);

      this.markerGroup.addLayer(newMarker);
    });
    if (markers.length) {
      this.map.fitBounds(this.markerGroup.getBounds(), { maxZoom: 17 });
    }
  }
}
