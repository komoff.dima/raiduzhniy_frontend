import { ComponentType } from '@angular/cdk/overlay';
import { Observable } from 'rxjs';
import { Dictionary, MinMax } from '../../interfaces';
import { SelectedFilterChip, SortingDropdownOptionsInput } from './components';
import { FilterType } from './filterable-content.enum';
import {
  MinMaxDateFilterInputs,
  MinMaxDateFilterValue,
  OptionsFilterInputs,
  SearchFilterInputs,
  SearchFilterValue,
} from './filters';
import { FilterBaseComponent } from './filters/filter-base.component';

export interface FilterableContentOptions {
  filters$: Observable<Filters>;
  sorting$?: Observable<SortingDropdownOptionsInput>;
}

export type Filters = Filter[];

export type FilterComponent<T = any> = ComponentType<FilterBaseComponent<T>>;

export interface TypeDependentInputs<Type, Inputs = {}> {
  type: Type;
  inputs?: Inputs;
}

export interface TypeDependentValue<Type, Value> {
  type: Type;
  value: Value;
}

export type Filter = {
  label: string;
  key: string;
} & (
  | TypeDependentInputs<FilterType.Search, SearchFilterInputs>
  | TypeDependentInputs<FilterType.Options, OptionsFilterInputs>
  | TypeDependentInputs<FilterType.DateRange, MinMaxDateFilterInputs>
);

export type FilterTypeMatcher<T> = {
  [key in FilterType]: T;
};

export type SelectedFilter =
  | TypeDependentValue<FilterType.Search, SearchFilterValue>
  | TypeDependentValue<FilterType.Options, OptionsFilterInputs>
  | TypeDependentValue<FilterType.DateRange, MinMaxDateFilterValue>;

export type SelectedFilters = Dictionary<SelectedFilter>;

export type AdaptedFilter =
  | TypeDependentValue<FilterType.Search, string>
  | TypeDependentValue<FilterType.Options, string[]>
  | TypeDependentValue<FilterType.DateRange, MinMax>;

export type AdaptedFilters = Dictionary<AdaptedFilter>;

export type ChipCreationFunc = (
  filterKey: string,
  selectedFilter: SelectedFilter
) => SelectedFilterChip[];

export type FilterAdapterFunc = (
  selectedFilter: SelectedFilter
) => AdaptedFilter;

export interface FilterMatcherSetup {
  component: FilterComponent;
  chipCreator: ChipCreationFunc;
  selectedFilterAdapter?: FilterAdapterFunc;
}
