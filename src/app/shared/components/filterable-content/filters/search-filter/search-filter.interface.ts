export type SearchFilterValue = string;

export interface SearchFilterInputs {
  label?: string;
}
