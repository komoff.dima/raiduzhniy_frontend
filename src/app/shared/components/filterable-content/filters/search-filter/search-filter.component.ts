import { CommonModule } from '@angular/common';
import { Component, Input } from '@angular/core';
import { FormBuilder, FormControl, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import {
  BehaviorSubject,
  debounce,
  distinctUntilChanged,
  map,
  timer,
  withLatestFrom,
} from 'rxjs';
import { FilterBaseComponent } from '../filter-base.component';
import {
  SearchFilterInputs,
  SearchFilterValue,
} from './search-filter.interface';

interface Bouncer {
  debounceNeeded: boolean;
  value: string;
}

@Component({
  selector: 'rdn-search-filter',
  standalone: true,
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
  ],
  templateUrl: './search-filter.component.html',
  styleUrls: ['./search-filter.component.scss'],
})
@UntilDestroy()
export class SearchFilterComponent
  extends FilterBaseComponent<SearchFilterValue>
  implements SearchFilterInputs
{
  searchFC = new FormControl<string>('');

  @Input() label!: string;

  private _bouncer$ = new BehaviorSubject<Bouncer>({
    debounceNeeded: true,
    value: '',
  });

  constructor(fb: FormBuilder) {
    super(fb);
    this.addFormControls({
      search: this.searchFC,
    });

    this.searchFC.valueChanges
      .pipe(
        untilDestroyed(this),
        withLatestFrom(this._bouncer$),
        debounce(([_, debounce]) => timer(debounce.debounceNeeded ? 500 : 0)),
        map(([value]) => value),
        distinctUntilChanged()
      )
      .subscribe(value => this.setFilterValue(value));
  }

  clearInput(): void {
    this._bouncer$.next({ debounceNeeded: false, value: '' });
    this.searchFC.setValue('');
    this._bouncer$.next({ debounceNeeded: true, value: '' });
  }

  override partiallyReset() {
    this.clearInput();
  }
}
