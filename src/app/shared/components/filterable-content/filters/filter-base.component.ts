import { Component, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Dictionary } from '../../../interfaces';

@Component({ template: '' })
export abstract class FilterBaseComponent<
  Value,
  PartiallyResetArgument = Value,
> {
  @Output() readonly filtered = new EventEmitter<Value>();

  protected form: FormGroup;

  protected constructor(fb: FormBuilder) {
    this.form = fb.group({});
  }

  reset(): void {
    this.form.reset(undefined, { emitEvent: false });
  }

  partiallyReset(formControlName: string): void {
    this.form.get(formControlName).reset();
  }

  protected setFilterValue(value: Value): void {
    this.filtered.emit(value);
  }

  protected addFormControls(controls: Dictionary<FormControl>): void {
    Object.entries(controls).forEach(([name, control]) =>
      this.form.addControl(name, control)
    );
  }
}
