import { Observable } from 'rxjs';

export type OptionsFilterValue = OptionsFilterOption[];

export interface OptionsFilterOption {
  key: string;
  label: string;
}

export interface OptionsFilterInputs {
  options: OptionsFilterOption[] | Observable<OptionsFilterOption[]>;
}

export interface FilterOptionFormControl {
  formControlName: string;
  label: string;
}
