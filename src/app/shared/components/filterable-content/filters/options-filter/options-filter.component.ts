import { Component, Input, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormBuilder, FormControl, ReactiveFormsModule } from '@angular/forms';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { map, Observable, take } from 'rxjs';
import { LoaderDirective } from '../../../../directives';
import { Dictionary } from '../../../../interfaces';
import { FilterBaseComponent } from '../filter-base.component';
import {
  FilterOptionFormControl,
  OptionsFilterInputs,
  OptionsFilterOption,
  OptionsFilterValue,
} from './options-filter.interface';

@Component({
  selector: 'rdn-options-filter',
  standalone: true,
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatCheckboxModule,
    LoaderDirective,
  ],
  templateUrl: './options-filter.component.html',
  styleUrls: ['./options-filter.component.scss'],
})
@UntilDestroy()
export class OptionsFilterComponent
  extends FilterBaseComponent<OptionsFilterValue, OptionsFilterOption>
  implements OnInit, Required<OptionsFilterInputs>
{
  @Input() options: OptionsFilterOption[] | Observable<OptionsFilterOption[]>;

  formControls: FilterOptionFormControl[] = [];

  optionsMap: Dictionary<OptionsFilterOption> = {};

  isLoading = false;

  constructor(fb: FormBuilder) {
    super(fb);
  }

  ngOnInit() {
    if (Array.isArray(this.options)) {
      this.generateFormControls(this.options);
    } else {
      this.isLoading = true;
      this.options.pipe(take(1), untilDestroyed(this)).subscribe(options => {
        this.generateFormControls(options);
        this.isLoading = false;
      });
    }
  }

  trackByName(index: number, fc: FilterOptionFormControl): string {
    return fc.formControlName;
  }

  private generateFormControls(options: OptionsFilterOption[]) {
    this.formControls = options.map(option => {
      this.addFormControls({ [option.key]: new FormControl<boolean>(false) });
      this.optionsMap[option.key] = option;

      return {
        label: option.label,
        formControlName: option.key,
      };
    });

    this.form.valueChanges
      .pipe(
        untilDestroyed(this),
        map(rawValue =>
          Object.entries(rawValue)
            .filter(([_, selected]) => selected)
            .map(([key]) => this.optionsMap[key])
        )
      )
      .subscribe(options => this.setFilterValue(options));
  }
}
