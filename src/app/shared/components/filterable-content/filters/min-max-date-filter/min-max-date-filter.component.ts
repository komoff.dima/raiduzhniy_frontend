import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormBuilder, FormControl, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { ORGANIZATION_FOUNDING_DATE } from '../../../../constants';
import { MinMax } from '../../../../interfaces';
import { FilterBaseComponent } from '../filter-base.component';
import {
  MinMaxDateFilterInputs,
  MinMaxDateFilterValue,
} from './min-max-date-filter.interface';

@Component({
  selector: 'rdn-min-max-date-filter',
  standalone: true,
  imports: [
    CommonModule,
    MatInputModule,
    MatDatepickerModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatIconModule,
  ],
  templateUrl: './min-max-date-filter.component.html',
  styleUrls: ['./min-max-date-filter.component.scss'],
})
export class MinMaxDateFilterComponent
  extends FilterBaseComponent<MinMaxDateFilterValue>
  implements Required<MinMaxDateFilterInputs>
{
  @Input() min: Date = ORGANIZATION_FOUNDING_DATE;

  @Input() max: Date = new Date();

  @Input() label!: string;

  minFC = new FormControl<Date>(null);

  maxFC = new FormControl<Date>(null);

  constructor(fb: FormBuilder) {
    super(fb);

    this.addFormControls({
      min: this.minFC,
      max: this.maxFC,
    });
  }

  apply(): void {
    if (this.formIsValid) {
      this.setFilterValue(this.form.getRawValue());
    }
  }

  get formIsValid(): boolean {
    const dateRange: MinMax<Date, Date> = this.form.getRawValue();

    return Object.values(dateRange).some(value => value);
  }

  resetForm(): void {
    this.reset();
    this.setFilterValue(null);
  }

  override partiallyReset() {
    this.resetForm();
  }
}
