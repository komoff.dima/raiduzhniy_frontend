import { MinMax } from '../../../../interfaces';

export interface MinMaxDateFilterInputs extends MinMax<Date, Date> {
  label?: string;
}

export interface MinMaxDateFilterValue extends MinMax<Date, Date> {}
