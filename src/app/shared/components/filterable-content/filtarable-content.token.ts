import { InjectionToken } from '@angular/core';
import { FilterableContentOptions } from './filterable-content.interface';

export const FILTERS_TOKEN = new InjectionToken<FilterableContentOptions>(
  'FILTERS_TOKEN'
);
