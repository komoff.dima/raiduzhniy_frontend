import { FilterType } from './filterable-content.enum';
import {
  FilterMatcherSetup,
  FilterTypeMatcher,
  SelectedFilter,
} from './filterable-content.interface';
import {
  MinMaxDateFilterValue,
  OptionsFilterValue,
  SearchFilterValue,
} from './filters';
import { MinMaxDateFilterComponent } from './filters/min-max-date-filter/min-max-date-filter.component';
import { OptionsFilterComponent } from './filters/options-filter/options-filter.component';
import { SearchFilterComponent } from './filters/search-filter/search-filter.component';

export const FILTER_TYPE_MATCHER: FilterTypeMatcher<FilterMatcherSetup> = {
  [FilterType.Search]: {
    component: SearchFilterComponent,
    chipCreator: (filterKey: string, selectedFilter: SelectedFilter) => {
      const value = selectedFilter.value as SearchFilterValue;

      return [
        {
          filterKey,
          label: value,
          value: value,
        },
      ];
    },
  },
  [FilterType.Options]: {
    component: OptionsFilterComponent,
    chipCreator: (filterKey: string, selectedFilter: SelectedFilter) => {
      const options = selectedFilter.value as OptionsFilterValue;

      return options.map(({ key, label }) => ({
        value: key,
        label,
        filterKey,
      }));
    },
    selectedFilterAdapter: (selectedFilter: SelectedFilter) => {
      const value = selectedFilter.value as OptionsFilterValue;

      return {
        type: selectedFilter.type as FilterType.Options,
        value: value.map(v => v.key),
      };
    },
  },
  [FilterType.DateRange]: {
    component: MinMaxDateFilterComponent,
    chipCreator: (filterKey: string, selectedFilter: SelectedFilter) => {
      const value = selectedFilter.value as MinMaxDateFilterValue;

      const [min, max] = [value.min, value.max].map(
        v => v?.toLocaleDateString().replaceAll('/', '.')
      );

      const label = `${min ? min : ''} - ${max ? max : ''}`;

      return [
        {
          filterKey,
          label,
        },
      ];
    },
    selectedFilterAdapter: (selectedFilter: SelectedFilter) => {
      const { max, min } = selectedFilter.value as MinMaxDateFilterValue;

      return {
        type: selectedFilter.type as FilterType.DateRange,
        value: {
          min: min?.toISOString(),
          max: max?.toISOString(),
        },
      };
    },
  },
};
