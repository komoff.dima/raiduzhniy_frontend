import { Pipe, PipeTransform } from '@angular/core';
import { FILTER_TYPE_MATCHER } from '../filterable-content.constant';
import { FilterType } from '../filterable-content.enum';
import { FilterComponent } from '../filterable-content.interface';

@Pipe({
  name: 'getFilterComponent',
  standalone: true,
})
export class GetFilterComponentPipe implements PipeTransform {
  transform(type: FilterType): FilterComponent {
    return FILTER_TYPE_MATCHER[type].component;
  }
}
