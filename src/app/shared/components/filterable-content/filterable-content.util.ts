import { SelectedFilterChip } from './components';
import { FILTER_TYPE_MATCHER } from './filterable-content.constant';
import {
  AdaptedFilter,
  AdaptedFilters,
  SelectedFilters,
} from './filterable-content.interface';

const inInvalidValue = (value: unknown): boolean =>
  !value || (Array.isArray(value) && !value.length);

export class FilterableContentUtil {
  static buildChips(selectedFilters: SelectedFilters): SelectedFilterChip[] {
    return Object.entries(selectedFilters)
      .map(([key, selectedFilter]) => {
        const value = selectedFilter.value;

        if (inInvalidValue(value)) {
          return [];
        }

        return FILTER_TYPE_MATCHER[selectedFilter.type].chipCreator(
          key,
          selectedFilter
        );
      })
      .flat(1);
  }

  static adaptSelectedFilters(
    selectedFilters: SelectedFilters
  ): AdaptedFilters {
    return Object.entries(selectedFilters).reduce(
      (adaptedFilters, [key, selectedFilter]) => {
        const adapter =
          FILTER_TYPE_MATCHER[selectedFilter.type].selectedFilterAdapter;

        if (!inInvalidValue(selectedFilter.value)) {
          if (adapter) {
            adaptedFilters[key] = adapter(selectedFilter);
          } else {
            adaptedFilters[key] = selectedFilter as AdaptedFilter;
          }
        }

        return adaptedFilters;
      },
      {} as AdaptedFilters
    );
  }
}
