import { CommonModule } from '@angular/common';
import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ComponentRef,
  EmbeddedViewRef,
  EventEmitter,
  Inject,
  Output,
  QueryList,
  TemplateRef,
  ViewChild,
  ViewChildren,
  ViewContainerRef,
} from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatIconModule } from '@angular/material/icon';
import { BreakpointsService } from '@core/services';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { BehaviorSubject, map, Observable, tap } from 'rxjs';
import { InsertComponentDirective, LoaderDirective } from '../../directives';
import { Breakpoints } from '../../enums';
import { Dictionary } from '../../interfaces';
import { CellComponentInputsPipe } from '../table/pipes/cell-component-inputs.pipe';
import {
  SelectedFilterChip,
  SortingDropdownOptionsInput,
  SortOption,
} from './components';
import { SelectedFilterChipsComponent } from './components/selected-filter-chips/selected-filter-chips.component';
import { SortingDropdownComponent } from './components/sorting-dropdown/sorting-dropdown.component';
import { FILTERS_TOKEN } from './filtarable-content.token';
import {
  AdaptedFilters,
  Filter,
  FilterableContentOptions,
  Filters,
  SelectedFilter,
  SelectedFilters,
} from './filterable-content.interface';
import { FilterableContentUtil } from './filterable-content.util';
import { FilterBaseComponent } from './filters/filter-base.component';
import { GetFilterComponentPipe } from './pipes/get-filter-component.pipe';

type FilterComponent = FilterBaseComponent<unknown>;

@Component({
  selector: 'rdn-filterable-content',
  standalone: true,
  imports: [
    CommonModule,
    MatExpansionModule,
    CellComponentInputsPipe,
    InsertComponentDirective,
    GetFilterComponentPipe,
    LoaderDirective,
    SelectedFilterChipsComponent,
    SortingDropdownComponent,
    MatButtonModule,
    MatIconModule,
  ],
  templateUrl: './filterable-content.component.html',
  styleUrls: ['./filterable-content.component.scss'],
})
@UntilDestroy()
export class FilterableContentComponent implements AfterViewInit {
  private filterComponentsMap: Dictionary<ComponentRef<FilterComponent>> = {};

  private _selectedFilterChips$ = new BehaviorSubject<SelectedFilterChip[]>([]);

  private _selectedFilters: SelectedFilters = {};

  filterChipsEmbeddedView: EmbeddedViewRef<unknown>;

  @Output() readonly filtered = new EventEmitter<AdaptedFilters>();

  @Output() readonly sorted = new EventEmitter<SortOption>();

  @ViewChildren(InsertComponentDirective)
  filterComponents: QueryList<InsertComponentDirective<FilterComponent>>;

  @ViewChild('chips', { read: TemplateRef })
  filterChipsTemplateRef: TemplateRef<unknown>;

  @ViewChild('headerFilterChipsPlaceholder', { read: ViewContainerRef })
  headerFilterChipsPlaceholderVC: ViewContainerRef;

  @ViewChild('asideFilterChipsPlaceholder', { read: ViewContainerRef })
  asideFilterChipsPlaceholderVC: ViewContainerRef;

  filters$: Observable<Filters>;

  sorting$: Observable<SortingDropdownOptionsInput>;

  selectedFilterChips$ = this._selectedFilterChips$.asObservable();

  isMobileView$: Observable<boolean> = this.breakpointsService
    .observeBreakpoint(Breakpoints.LG)
    .pipe(map(matched => !matched));

  set selectedFilters(selectedFilters: SelectedFilters) {
    this._selectedFilters = selectedFilters;
    this._selectedFilterChips$.next(
      FilterableContentUtil.buildChips(selectedFilters)
    );
    this.filtered.next(
      FilterableContentUtil.adaptSelectedFilters(selectedFilters)
    );
  }

  get selectedFilters(): SelectedFilters {
    return this._selectedFilters;
  }

  isFiltersLoading = false;

  isFilterStackHidden = true;

  constructor(
    @Inject(FILTERS_TOKEN) options: FilterableContentOptions,
    private breakpointsService: BreakpointsService,
    private cd: ChangeDetectorRef
  ) {
    this.filters$ = options.filters$.pipe(
      tap(() => (this.isFiltersLoading = false))
    );
    this.sorting$ = options.sorting$;
  }

  trackByFilters(index: number, filter: Filter): string {
    return filter.key;
  }

  reset(): void {
    Object.values(this.filterComponentsMap).forEach(compRef => {
      compRef.instance.reset();
    });

    this.selectedFilters = {};
  }

  ngAfterViewInit() {
    this.filterComponents.forEach(c => {
      const filter = c.rdnInsertComponentSharedData as Filter;
      this.filterComponentsMap[filter.key] = c.componentRef;

      c.componentRef.instance.filtered
        .pipe(untilDestroyed(this))
        .subscribe(value => {
          this.selectedFilters = {
            ...this.selectedFilters,
            [filter.key]: {
              type: filter.type,
              value,
            } as SelectedFilter,
          };
        });
    });

    this.createFilterChipsEmbeddedView();

    this.observeMobileView();

    this.cd.detectChanges();
  }

  discardFilter(filter: SelectedFilterChip): void {
    this.filterComponentsMap[filter.filterKey].instance.partiallyReset(
      filter.value
    );
  }

  toggleFilterStack() {
    this.isFilterStackHidden = !this.isFilterStackHidden;
  }

  private createFilterChipsEmbeddedView() {
    this.filterChipsEmbeddedView =
      this.filterChipsTemplateRef.createEmbeddedView({
        chips$: this.selectedFilterChips$,
      });
  }

  private observeMobileView() {
    this.isMobileView$.pipe(untilDestroyed(this)).subscribe(isMobileView => {
      if (isMobileView) {
        this.asideFilterChipsPlaceholderVC.insert(this.filterChipsEmbeddedView);
      } else {
        this.headerFilterChipsPlaceholderVC.insert(
          this.filterChipsEmbeddedView
        );
        this.isFilterStackHidden = true;
      }
    });
  }
}
