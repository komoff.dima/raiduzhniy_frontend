import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatChipsModule } from '@angular/material/chips';
import { MatIconModule } from '@angular/material/icon';
import { SelectedFilterChip } from './selected-filter-chips.interface';

@Component({
  selector: 'rdn-selected-filter-chips',
  standalone: true,
  imports: [CommonModule, MatChipsModule, MatIconModule, MatButtonModule],
  templateUrl: './selected-filter-chips.component.html',
  styleUrls: ['./selected-filter-chips.component.scss'],
})
export class SelectedFilterChipsComponent {
  @Input() chips: SelectedFilterChip[];

  @Output() readonly filtersReset = new EventEmitter<void>();

  @Output() readonly filterDiscarded = new EventEmitter<SelectedFilterChip>();

  trackByFn(index: number, chip: SelectedFilterChip): string {
    return `${chip.filterKey}-${chip.value}`;
  }

  discardFilter(chip: SelectedFilterChip): void {
    this.filterDiscarded.emit(chip);
  }

  resetFilters(): void {
    this.filtersReset.emit();
  }
}
