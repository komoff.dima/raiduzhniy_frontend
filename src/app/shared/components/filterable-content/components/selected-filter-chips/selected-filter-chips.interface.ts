export interface SelectedFilterChip {
  filterKey: string;
  value?: string;
  label: string;
}
