import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  Pipe,
  PipeTransform,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  ReactiveFormsModule,
} from '@angular/forms';
import { MAT_FORM_FIELD_DEFAULT_OPTIONS } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Dictionary } from '../../../../interfaces';
import {
  SortOption,
  SortingDropdownOptionInput,
  SortingDropdownOptionsInput,
} from './sorting-dropdown.interface';

const generateFormValue = (option: SortOption): string => {
  return `${option.key}_${option.direction}`;
};

@Pipe({
  name: 'generateFormValue',
  standalone: true,
})
export class GenerateFormValuePipe implements PipeTransform {
  transform(option: SortOption): string {
    return generateFormValue(option);
  }
}

@Component({
  selector: 'rdn-sorting-dropdown',
  standalone: true,
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatInputModule,
    MatSelectModule,
    GenerateFormValuePipe,
  ],
  templateUrl: './sorting-dropdown.component.html',
  styleUrls: ['./sorting-dropdown.component.scss'],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        subscriptSizing: 'dynamic',
      },
    },
  ],
})
@UntilDestroy()
export class SortingDropdownComponent implements OnInit {
  @Input() sortingOptions: SortingDropdownOptionsInput = [];

  @Output() readonly sorted = new EventEmitter<SortOption>();

  form: FormGroup;

  private readonly sortingFC = new FormControl('');

  constructor(fb: FormBuilder) {
    this.form = fb.group({
      sortingValue: this.sortingFC,
    });
  }

  ngOnInit() {
    const sortingOptionsMapper: Dictionary<SortingDropdownOptionInput> = {};

    this.sortingOptions.forEach(option => {
      const value = generateFormValue(option);

      if (option.initial) {
        this.sortingFC.setValue(value);
      }

      sortingOptionsMapper[value] = option;
    });

    this.sortingFC.valueChanges.pipe(untilDestroyed(this)).subscribe(value => {
      const { initial, ...option } = sortingOptionsMapper[value];
      this.sorted.emit(option);
    });
  }

  trackByFn(index: number) {
    return index;
  }
}
