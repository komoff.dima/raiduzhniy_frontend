import { OrderDirection } from '../../../../interfaces';

export interface SortOption {
  key: string;
  label: string;
  direction: OrderDirection;
}

export interface SortingDropdownOptionInput extends SortOption {
  initial?: boolean;
}

export type SortingDropdownOptionsInput = SortingDropdownOptionInput[];
