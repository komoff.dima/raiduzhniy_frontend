export enum FilterType {
  Search = 'search',
  Options = 'options',
  DateRange = 'date-range',
}
