import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BASIC_DATE_FORMAT } from '../../../constants';
import { TableCell } from '../../table';

@Component({
  standalone: true,
  imports: [CommonModule],
  template: `{{ (value | date: format) || '-//-' }}`,
})
export class DateCellComponent<T> implements TableCell<T> {
  @Input() row: T;
  @Input() value: string;
  // https://angular.io/api/common/DatePipe
  @Input() format: string = BASIC_DATE_FORMAT;
}
