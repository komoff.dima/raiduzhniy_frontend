import { CommonModule } from '@angular/common';
import { Component, Input, ViewEncapsulation } from '@angular/core';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatListModule } from '@angular/material/list';
import { RouterLink, RouterLinkActive } from '@angular/router';
import { ForRolesDirective } from '../../directives';
import { RoutesAccordion } from './routes-accordion.interface';

@Component({
  selector: 'rdn-routes-accordion',
  standalone: true,
  imports: [
    CommonModule,
    MatExpansionModule,
    MatListModule,
    RouterLinkActive,
    RouterLink,
    ForRolesDirective,
  ],
  templateUrl: './routes-accordion.component.html',
  styleUrls: ['./routes-accordion.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class RoutesAccordionComponent {
  activeLinkIndex: number;

  @Input() routesAccordion: RoutesAccordion[] = [];

  changedActiveLink(isActive: boolean, index: number) {
    if (isActive) {
      setTimeout(() => (this.activeLinkIndex = index), 0);
    }
  }

  trackByFn(index: number): number {
    return index;
  }
}
