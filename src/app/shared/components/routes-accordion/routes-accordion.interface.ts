import { Data, Route } from '@angular/router';
import { UserRole } from '../../enums';

export interface RoutesAccordionData extends Data {
  label: string;
  subRoutes?: Route[];
  forRoles?: UserRole[];
}

export interface RoutesAccordion extends Route {
  data: RoutesAccordionData;
}
