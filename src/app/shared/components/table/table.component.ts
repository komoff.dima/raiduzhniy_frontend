import {
  AfterViewInit,
  Component,
  EventEmitter,
  Inject,
  Input,
  Output,
  ViewChild,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatPaginator, MatPaginatorModule } from '@angular/material/paginator';
import { MatSort, MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatTooltipModule } from '@angular/material/tooltip';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { merge, startWith } from 'rxjs';
import { TableLocalStorageClass } from '../../abstract-classes';
import { InsertComponentDirective } from '../../directives';
import { OrderDirection, TableData, TableQueryParams } from '../../interfaces';
import { CellComponentInputsPipe } from './pipes/cell-component-inputs.pipe';
import { ValueAdapterPipe } from './pipes/value-adapter.pipe';
import { TableAction } from './table.enum';
import {
  TableActions,
  TableColumnSettings,
  TableSettings,
} from './table.interface';
import { TABLE_SETTINGS } from './table.token';

@Component({
  selector: 'rdn-table',
  standalone: true,
  imports: [
    CommonModule,
    MatButtonModule,
    MatIconModule,
    MatPaginatorModule,
    MatSortModule,
    MatTableModule,
    MatTooltipModule,
    InsertComponentDirective,
    ValueAdapterPipe,
    CellComponentInputsPipe,
  ],
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
})
@UntilDestroy()
export class TableComponent<T>
  extends TableLocalStorageClass
  implements AfterViewInit
{
  readonly actions?: TableActions;

  readonly columns: TableColumnSettings<T>[];

  readonly TABLE_ACTIONS = TableAction;

  readonly displayedColumns: string[];

  @Input() data: TableData<T>;

  @Output() readonly sortOrPaginationChanged =
    new EventEmitter<TableQueryParams>();

  @Output() readonly edited = new EventEmitter<T>();

  @Output() readonly deleted = new EventEmitter<T>();

  @ViewChild(MatPaginator) paginator: MatPaginator;

  @ViewChild(MatSort) sort: MatSort;

  constructor(
    @Inject(TABLE_SETTINGS)
    {
      columns,
      initialQueryParams,
      queryParamsStorageKey,
      actions,
    }: TableSettings<T>
  ) {
    super(initialQueryParams, queryParamsStorageKey);
    this.actions = actions;
    this.columns = columns;

    this.displayedColumns = [
      ...(this.actions ? ['actions'] : []),
      ...columns.map(column => column.name),
    ];
  }

  ngAfterViewInit() {
    merge(this.sort.sortChange, this.paginator.page)
      .pipe(startWith(null), untilDestroyed(this))
      .subscribe(() => {
        setTimeout(() => {
          this.queryParams = {
            orderBy: this.sort.active,
            orderDirection: this.sort.direction as OrderDirection,
            pageSize: this.paginator.pageSize,
            pageNumber: this.paginator.pageIndex + 1,
          };

          this.sortOrPaginationChanged.emit(this.queryParams);
        });
      });
  }

  trackByColumns(index: number, column: TableColumnSettings<T>): string {
    return column.name;
  }
}
