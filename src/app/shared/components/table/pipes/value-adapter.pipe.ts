import { Pipe, PipeTransform } from '@angular/core';
import { CellValueAdapter } from '../table.interface';

@Pipe({
  name: 'valueAdapter',
  standalone: true,
})
export class ValueAdapterPipe implements PipeTransform {
  transform<T>(
    value: unknown,
    adapter: CellValueAdapter<T>,
    rowData: T
  ): string {
    return adapter(value, rowData);
  }
}
