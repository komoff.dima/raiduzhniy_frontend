import { Pipe, PipeTransform } from '@angular/core';
import { Dictionary } from '../../../interfaces';

@Pipe({
  name: 'cellComponentInputs',
  standalone: true,
})
export class CellComponentInputsPipe implements PipeTransform {
  transform<RowData>(
    row: RowData,
    value: unknown,
    inputs: Dictionary<unknown> = {}
  ): Dictionary<unknown> {
    return { row, value, ...inputs };
  }
}
