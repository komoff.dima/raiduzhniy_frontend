import { InjectionToken } from '@angular/core';

export const TABLE_SETTINGS = new InjectionToken('TABLE_SETTINGS');
