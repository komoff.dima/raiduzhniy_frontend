import { ComponentType } from '@angular/cdk/overlay';
import { TooltipPosition } from '@angular/material/tooltip';
import { Dictionary, TableQueryParams } from '../../interfaces';
import { TableAction } from './table.enum';

export interface TableActionSettings {
  tooltip?: string;
  tooltipPosition?: TooltipPosition;
}

export type TableActions = Partial<Record<TableAction, TableActionSettings>>;

export type CellValueAdapter<T> = (value: any, data: T) => string;

export interface TableComponentSettings {
  class: ComponentType<unknown>;
  inputs?: Dictionary<any>;
}

export interface ColumnHeaderOptions {
  sortHeader?: boolean;
  disableClear?: boolean;
}

export interface TableColumnSettings<T> {
  name: string;
  label: string;
  header?: ColumnHeaderOptions;
  valueAdapter?: CellValueAdapter<T>;
  component?: TableComponentSettings;
  sortHeader?: boolean;
}

export interface TableSettings<T> {
  queryParamsStorageKey: string;
  actions?: TableActions;
  columns: TableColumnSettings<T>[];
  initialQueryParams: TableQueryParams;
}

export interface TableCell<T> {
  row: T;
  value: unknown;
}
