export enum Breakpoints {
  XS = '--bs-breakpoint-xs',
  SM = '--bs-breakpoint-sm',
  MD = '--bs-breakpoint-md',
  LG = '--bs-breakpoint-lg',
  XL = '--bs-breakpoint-xl',
  XXL = '--bs-breakpoint-xxl',
}
