export class FormDataUtil {
  static createFormDataFromObject(object: { [key: string]: any }): FormData {
    const fd = new FormData();

    Object.entries(object).forEach(([key, value]) => {
      if (value) {
        if (Array.isArray(value)) {
          return value.forEach(v => fd.append(key, v));
        }
        fd.append(key, value);
      }
    });

    return fd;
  }
}
