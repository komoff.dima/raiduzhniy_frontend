import {
  AbstractControl,
  AsyncValidatorFn,
  ValidationErrors,
} from '@angular/forms';
import { catchError, map, Observable, of, switchMap, timer } from 'rxjs';
import { DefaultResponse, UniqueValidator } from '../interfaces';

export interface UniqueAsyncValidatorOptions {
  debounceTime: number;
}

const DEFAULT_OPTIONS: UniqueAsyncValidatorOptions = {
  debounceTime: 300,
};

export class UniqueAsyncValidator {
  static createValidator<Service extends UniqueValidator>(
    validationField: string,
    service: Service,
    options?: Partial<UniqueAsyncValidatorOptions>
  ): AsyncValidatorFn {
    const { debounceTime }: UniqueAsyncValidatorOptions = {
      ...DEFAULT_OPTIONS,
      ...options,
    };

    return (control: AbstractControl): Observable<ValidationErrors> =>
      timer(debounceTime).pipe(
        switchMap(() =>
          service.validateUnique({ [validationField]: control.value }).pipe(
            catchError(() => of({ success: false })),
            map(({ success }: DefaultResponse) =>
              success ? null : { isNotUnique: true }
            )
          )
        )
      );
  }
}
