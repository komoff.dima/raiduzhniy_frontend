import { EnvironmentProviders, Provider } from '@angular/core';
import { provideEffects } from '@ngrx/effects';
import { provideState } from '@ngrx/store';
import { AboutUsService } from '@shared/services';
import { AboutUsEffects } from './about-us.effects';
import { AboutUsFacade } from './about-us.facade';
import { aboutUsReducer } from './about-us.reducer';

export const AboutUsStateProviders: (Provider | EnvironmentProviders)[] = [
  provideEffects(AboutUsEffects),
  provideState({ name: 'aboutUs', reducer: aboutUsReducer }),
  AboutUsService,
  AboutUsFacade,
];
