import { inject, Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { ContactsService } from '@shared/services/contacts.service';
import * as ContactsActions from '@state/contacts/contacts.actions';
import { catchError, map, mergeMap, of, tap } from 'rxjs';

@Injectable()
export class ContactsEffects {
  private contactsService = inject(ContactsService);
  private actions$ = inject(Actions);

  getContacts$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ContactsActions.getContacts),
      mergeMap(() => {
        return this.contactsService.getHtml().pipe(
          map(response => ContactsActions.getContactsSuccess({ response })),
          catchError(error =>
            of(ContactsActions.getContactsFailure({ error: error.message }))
          )
        );
      })
    );
  });

  updateContacts$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ContactsActions.updateContacts),
      mergeMap(({ dto }) => {
        return this.contactsService.updateHtml(dto).pipe(
          map(response => ContactsActions.getContactsSuccess({ response })),
          catchError(error =>
            of(ContactsActions.getContactsFailure({ error: error.message }))
          )
        );
      })
    );
  });

  createContacts$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ContactsActions.createContacts),
      mergeMap(({ dto }) => {
        return this.contactsService.createHtml(dto).pipe(
          map(response => ContactsActions.getContactsSuccess({ response })),
          catchError(error =>
            of(ContactsActions.getContactsFailure({ error: error.message }))
          )
        );
      })
    );
  });

  getMapsMarkers$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ContactsActions.getMarkers),
      mergeMap(() =>
        this.contactsService.getMapsMarkers().pipe(
          map(tableData =>
            ContactsActions.getMarkersSuccess({ markers: tableData.elements })
          ),
          catchError(error =>
            of(ContactsActions.getMarkersFailure({ error: error.message }))
          )
        )
      )
    );
  });

  deleteMarker$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ContactsActions.deleteMarker),
      mergeMap(({ markerId }) =>
        this.contactsService
          .deleteMapMarker(markerId)
          .pipe(map(() => ContactsActions.getMarkers()))
      )
    );
  });

  addMarker$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ContactsActions.addMarker),
      mergeMap(({ marker }) =>
        this.contactsService
          .addMapMarker(marker)
          .pipe(map(() => ContactsActions.getMarkers()))
      )
    );
  });
}
