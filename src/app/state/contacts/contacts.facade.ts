import { Injectable } from '@angular/core';
import { AppStateInterface } from '@core/types/app-state.interface';
import { Store } from '@ngrx/store';
import {
  Html,
  IMarker,
  MapMarkerDto,
  MapMarkerResponse,
} from '@shared/interfaces';
import {
  addMarker,
  createContacts,
  deleteMarker,
  getContacts,
  getMarkers,
  updateContacts,
} from '@state/contacts/contacts.actions';
import {
  selectContacts,
  selectIsLoadingContacts,
  selectIsLoadingMarkers,
  selectMarkers,
  selectMarkersError,
} from '@state/contacts/contacts.selector';

@Injectable()
export class ContactsFacade {
  contacts$ = this.store.select(selectContacts);
  isLoading$ = this.store.select(selectIsLoadingContacts);
  error$ = this.store.select(selectContacts);
  markers$ = this.store.select(selectMarkers);
  isLoadingMarkers$ = this.store.select(selectIsLoadingMarkers);
  markersError$ = this.store.select(selectMarkersError);

  constructor(private store: Store<AppStateInterface>) {}

  dispatchGetContacts(): void {
    this.store.dispatch(getContacts());
  }

  dispatchEditContacts(dto: Html): void {
    this.store.dispatch(updateContacts({ dto }));
  }

  dispatchCreateContacts(dto: Html): void {
    this.store.dispatch(createContacts({ dto }));
  }

  dispatchGetMarkers(): void {
    this.store.dispatch(getMarkers());
  }

  dispatchAddMarker(marker: MapMarkerDto) {
    this.store.dispatch(addMarker({ marker }));
  }

  dispatchRemoveMarker(marker: MapMarkerResponse): void {
    this.store.dispatch(deleteMarker({ markerId: marker.id }));
  }
}
