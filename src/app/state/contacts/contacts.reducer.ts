import { createReducer, on } from '@ngrx/store';
import * as ContactsActions from './contacts.actions';
import { ContactsStateInterface } from '@state/contacts/contacts-state.interface';

export const initialState: ContactsStateInterface = {
  markers: [],
  isMarkersLoading: false,
  markersError: null,
  isLoading: false,
  contacts: undefined,
  error: null,
};

export const contactsReducer = createReducer(
  initialState,
  on(ContactsActions.getContacts, state => ({
    ...state,
    isLoading: true,
  })),
  on(ContactsActions.updateContacts, state => ({
    ...state,
    isLoading: true,
  })),
  on(ContactsActions.createContacts, state => ({
    ...state,
    isLoading: true,
  })),
  on(ContactsActions.getContactsSuccess, (state, action) => ({
    ...state,
    isLoading: false,
    contacts: action.response,
  })),
  on(ContactsActions.getContactsFailure, (state, action) => ({
    ...state,
    isLoading: false,
    error: action.error,
  })),
  on(ContactsActions.getMarkers, state => ({
    ...state,
    isMarkersLoading: true,
  })),
  on(ContactsActions.getMarkersSuccess, (state, action) => ({
    ...state,
    isMarkersLoading: false,
    markers: action.markers,
  })),
  on(ContactsActions.getMarkersFailure, (state, action) => ({
    ...state,
    isMarkersLoading: false,
    error: action.error,
  })),
  on(ContactsActions.deleteMarker, state => ({
    ...state,
    isMarkersLoading: true,
  })),
  on(ContactsActions.addMarker, state => ({
    ...state,
    isMarkersLoading: true,
  }))
);
