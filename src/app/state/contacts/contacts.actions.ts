import { createAction, props } from '@ngrx/store';
import { Html, MapMarkerDto, MapMarkerResponse } from '@shared/interfaces';

const IDENTIFIER = 'Contacts';

export const getContacts = createAction(`[${IDENTIFIER}] Get Contacts`);

export const updateContacts = createAction(
  `[${IDENTIFIER}] Update Contacts`,
  props<{ dto: Html }>()
);

export const createContacts = createAction(
  `[${IDENTIFIER}] Create Contacts`,
  props<{ dto: Html }>()
);

export const getContactsSuccess = createAction(
  `[${IDENTIFIER}] Get Contacts Success`,
  props<{ response: Html }>()
);

export const getContactsFailure = createAction(
  `[${IDENTIFIER}] Get Contacts Failure`,
  props<{ error: string }>()
);

export const getMarkers = createAction(`[${IDENTIFIER}] Get Markers`);

export const getMarkersSuccess = createAction(
  `[${IDENTIFIER}] Get Markers Success`,
  props<{ markers: MapMarkerResponse[] }>()
);

export const getMarkersFailure = createAction(
  `[${IDENTIFIER}] Get Markers Failure`,
  props<{ error: string }>()
);

export const deleteMarker = createAction(
  `[${IDENTIFIER}] Delete marker`,
  props<{ markerId: string }>()
);

export const deleteMarkerSuccess = createAction(
  `[${IDENTIFIER}] Delete marker success`
);

export const deleteMarkerFailure = createAction(
  `[${IDENTIFIER}] Delete marker failure`,
  props<{ error: string }>()
);

export const addMarker = createAction(
  `[${IDENTIFIER}] Add marker`,
  props<{ marker: MapMarkerDto }>()
);

export const addMarkerSuccess = createAction(
  `[${IDENTIFIER}] Add marker success`,
  props<{ marker: MapMarkerResponse }>()
);

export const addMarkerFailure = createAction(
  `[${IDENTIFIER}] Add marker failure`,
  props<{ error: string }>()
);
