import { Html, MapMarkerResponse } from '@shared/interfaces';

export interface ContactsStateInterface {
  isLoading: boolean;
  contacts: Html;
  error: string;
  markers: MapMarkerResponse[];
  isMarkersLoading: boolean;
  markersError: string;
}
