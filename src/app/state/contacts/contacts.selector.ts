import { AppStateInterface } from '@core/types/app-state.interface';
import { createSelector } from '@ngrx/store';

export const selectFeature = (state: AppStateInterface) => state.contacts;

export const selectIsLoadingContacts = createSelector(
  selectFeature,
  state => state.isLoading
);

export const selectContacts = createSelector(
  selectFeature,
  state => state.contacts
);

export const selectContactsError = createSelector(
  selectFeature,
  state => state.error
);

export const selectIsLoadingMarkers = createSelector(
  selectFeature,
  state => state.isMarkersLoading
);

export const selectMarkers = createSelector(
  selectFeature,
  state => state.markers
);

export const selectMarkersError = createSelector(
  selectFeature,
  state => state.markersError
);
