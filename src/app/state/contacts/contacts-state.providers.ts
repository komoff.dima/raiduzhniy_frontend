import { EnvironmentProviders, Provider } from '@angular/core';
import { provideEffects } from '@ngrx/effects';
import { provideState } from '@ngrx/store';
import { ContactsService } from '@shared/services';
import { ContactsEffects } from './contacts.effects';
import { ContactsFacade } from './contacts.facade';
import { contactsReducer } from './contacts.reducer';

export const ContactsStateProviders: (Provider | EnvironmentProviders)[] = [
  provideEffects(ContactsEffects),
  provideState({ name: 'contacts', reducer: contactsReducer }),
  ContactsService,
  ContactsFacade,
];
