export interface UpdatesStateInterface {
  [collection: string]: boolean;
}
