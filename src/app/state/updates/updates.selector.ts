import { createSelector } from '@ngrx/store';
import { AppStateInterface } from '@core/types/app-state.interface';

export const selectFeature = (state: AppStateInterface) => state.updates;

export const selectUpdates = createSelector(selectFeature, state => state);
