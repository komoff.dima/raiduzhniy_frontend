import { createReducer, on } from '@ngrx/store';
import { UpdatesStateInterface } from './updates-state.interface';
import * as UpdatesActions from './updates.actions';

export const initialState: UpdatesStateInterface = {};

export const updatesReducer = createReducer(
  initialState,
  on(
    UpdatesActions.addUpdate,
    (state, props): UpdatesStateInterface => ({
      ...state,
      [props.update.collection]: true,
    })
  ),
  on(
    UpdatesActions.removeUpdate,
    (state, props): UpdatesStateInterface => ({
      ...state,
      [props.update]: false,
    })
  )
);
