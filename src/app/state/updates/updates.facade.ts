import { Injectable } from '@angular/core';
import { AppStateInterface } from '@core/types/app-state.interface';
import { Store } from '@ngrx/store';
import { Update } from '@shared/interfaces';
import * as UpdatesActions from './updates.actions';
import { selectUpdates } from './updates.selector';

@Injectable({
  providedIn: 'root',
})
export class UpdatesFacade {
  updates$ = this.store.select(selectUpdates);

  constructor(private store: Store<AppStateInterface>) {}

  dispatchAddUpdate(update: Update): void {
    this.store.dispatch(UpdatesActions.addUpdate({ update }));
  }

  dispatchRemoveUpdate(update: string): void {
    this.store.dispatch(UpdatesActions.removeUpdate({ update }));
  }
}
