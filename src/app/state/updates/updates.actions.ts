import { createAction, props } from '@ngrx/store';
import { Update } from '@shared/interfaces';

const IDENTIFIER = 'Updates';

export const addUpdate = createAction(
  `[${IDENTIFIER}] Add Update`,
  props<{ update: Update }>()
);

export const removeUpdate = createAction(
  `[${IDENTIFIER}] Remove Update`,
  props<{ update: string }>()
);
