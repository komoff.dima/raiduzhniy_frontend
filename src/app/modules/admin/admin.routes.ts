import { Route } from '@angular/router';
import { forRolesGuard } from '@core/guards/for-roles.guard';
import { provideEffects } from '@ngrx/effects';
import { provideState } from '@ngrx/store';
import { RoutesAccordion } from '@shared/components/routes-accordion/routes-accordion.interface';
import { UserRole } from '@shared/enums';
import { NewsService } from '@shared/services';
import { AboutUsStateProviders } from '@state/about-us/about-us-state.providers';
import { ContactsStateProviders } from '@state/contacts';
import { AdminComponent } from './admin.component';
import { ADMIN_ABOUT_US_SUB_ROUTES } from './pages/admin-about-us/admin-about-us.routes';
import { ADMIN_CONTACTS_SUB_ROUTES } from './pages/admin-contacts/admin-contacts.routes';
import { ADMIN_DOCUMENTS_SUB_ROUTES } from './pages/admin-documents/admin-documents.routes';
import { AdminDocumentTypesStateProviders } from './pages/admin-documents/pages/document-types/state/admin-document-types-state.providers';
import { AdminDocumentsStateProviders } from './pages/admin-documents/state/admin-documents-state.providers';
import { ADMIN_NEWS_SUB_ROUTES } from './pages/admin-news/admin-news.routes';
import {
  AdminNewsEffects,
  AdminNewsFacade,
  adminNewsReducer,
} from './pages/admin-news/state';

export const ADMIN_CHILDREN_ROUTES: RoutesAccordion[] = [
  // {
  //   path: 'photos',
  //   loadChildren: () =>
  //     import('./pages/admin-photos/admin-photos-routes').then(
  //       mod => mod.ADMIN_PHOTOS_ROUTES
  //     ),
  //   canActivate: [forRolesGuard],
  //   data: {
  //     label: 'Фотографії',
  //     subRoutes: ADMIN_PHOTOS_SUB_ROUTES,
  //     forRoles: [UserRole.Admin, UserRole.Superadmin],
  //   },
  // },
  {
    path: 'news',
    loadChildren: () =>
      import('./pages/admin-news/admin-news.routes').then(
        mod => mod.ADMIN_NEWS_ROUTES
      ),
    canActivate: [forRolesGuard],
    data: {
      label: 'Новини',
      subRoutes: ADMIN_NEWS_SUB_ROUTES,
      forRoles: [UserRole.Admin, UserRole.Superadmin],
    },
    providers: [
      provideEffects(AdminNewsEffects),
      provideState({ name: 'adminNews', reducer: adminNewsReducer }),
      NewsService,
      AdminNewsFacade,
    ],
  },
  {
    path: 'documents',
    loadChildren: () =>
      import('./pages/admin-documents/admin-documents.routes').then(
        mod => mod.ADMIN_DOCUMENTS_ROUTES
      ),
    canActivate: [forRolesGuard],
    data: {
      label: 'Документи',
      subRoutes: ADMIN_DOCUMENTS_SUB_ROUTES,
      forRoles: [UserRole.Admin, UserRole.Superadmin],
    },
    providers: [
      ...AdminDocumentTypesStateProviders,
      ...AdminDocumentsStateProviders,
    ],
  },
  {
    path: 'about-us',
    loadChildren: () =>
      import('./pages/admin-about-us/admin-about-us.routes').then(
        mod => mod.ADMIN_ABOUT_US_ROUTES
      ),
    canActivate: [forRolesGuard],
    data: {
      label: 'Про нас',
      subRoutes: ADMIN_ABOUT_US_SUB_ROUTES,
      forRoles: [UserRole.Admin, UserRole.Superadmin],
    },
    providers: [...AboutUsStateProviders],
  },
  {
    path: 'contacts',
    loadChildren: () =>
      import('./pages/admin-contacts/admin-contacts.routes').then(
        mod => mod.ADMIN_CONTACTS_ROUTES
      ),
    canActivate: [forRolesGuard],
    data: {
      label: 'Контакти',
      subRoutes: ADMIN_CONTACTS_SUB_ROUTES,
      forRoles: [UserRole.Admin, UserRole.Superadmin],
    },
    providers: [...ContactsStateProviders],
  },
];

export const ADMIN_ROUTES: Route[] = [
  {
    path: '',
    component: AdminComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'news',
      },
      ...ADMIN_CHILDREN_ROUTES,
    ],
  },
];
