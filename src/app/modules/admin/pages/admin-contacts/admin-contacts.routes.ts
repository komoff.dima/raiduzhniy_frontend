import { Route, Routes } from '@angular/router';
import { AdminContactsComponent } from './admin-contacts.component';

export const ADMIN_CONTACTS_SUB_ROUTES: Routes = [
  {
    path: '',
    component: AdminContactsComponent,
    data: {
      label: 'Адміністрування',
    },
  },
];

export const ADMIN_CONTACTS_ROUTES: Route[] = [...ADMIN_CONTACTS_SUB_ROUTES];
