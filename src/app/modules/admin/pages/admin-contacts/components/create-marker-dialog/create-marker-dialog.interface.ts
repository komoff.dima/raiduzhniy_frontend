export interface CreateMarkerDialogData {}

export interface CreateMarkerDialogForm {
  markerName: string;
}
