import { Component, Inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import {
  MAT_DIALOG_DATA,
  MatDialogModule,
  MatDialogRef,
} from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { CreateMarkerDialogData } from './create-marker-dialog.interface';

@Component({
  selector: 'rdn-create-marker-dialog',
  standalone: true,
  imports: [
    CommonModule,
    MatButtonModule,
    MatDialogModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
  ],
  templateUrl: './create-marker-dialog.component.html',
  styleUrls: ['./create-marker-dialog.component.scss'],
})
export class CreateMarkerDialogComponent {
  form: FormGroup;
  markerNameFC = new FormControl('', [Validators.required]);
  constructor(
    public dialogRef: MatDialogRef<CreateMarkerDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: CreateMarkerDialogData,
    fb: FormBuilder
  ) {
    this.form = fb.group({
      markerName: this.markerNameFC,
    });
  }

  createMarker() {
    if (this.form.valid) {
      this.dialogRef.close(this.form.getRawValue());
    }
  }
}
