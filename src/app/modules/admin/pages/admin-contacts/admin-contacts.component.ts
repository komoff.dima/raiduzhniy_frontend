import { CommonModule } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { HtmlEditorComponent, MapComponent } from '@shared/components';
import { ConfirmDialogComponent } from '@shared/components/dialogs';
import { ConfirmDialogData } from '@shared/components/dialogs/confirm-dialog/confirm-dialog.interface';
import { COMMON_DIALOG_CONFIG } from '@shared/constants';
import { EditHtmlEvent, IMarker, MapMarkerResponse } from '@shared/interfaces';
import { Html } from '@shared/interfaces';
import { ContactsFacade } from '@state/contacts';
import { LeafletMouseEvent } from 'leaflet';
import { filter, map, take } from 'rxjs';
import { CreateMarkerDialogForm } from './components/create-marker-dialog';
import { CreateMarkerDialogComponent } from './components/create-marker-dialog';

interface Listener {
  element: HTMLElement;
  listener: () => any;
  type: string;
}

@Component({
  selector: 'rdn-admin-contacts',
  standalone: true,
  imports: [CommonModule, HtmlEditorComponent, MapComponent, MatDialogModule],
  templateUrl: './admin-contacts.component.html',
  styleUrls: ['./admin-contacts.component.scss'],
})
@UntilDestroy()
export class AdminContactsComponent implements OnInit, OnDestroy {
  private readonly markersListeners: Listener[] = [];

  markers$ = this.contactsFacade.markers$.pipe(
    map(markers =>
      markers.map(marker => ({
        ...marker,
        content: () => {
          const div = document.createElement('div');
          const button = this.createRemoveMarkerButton(marker);
          div.innerHTML = `
            <h5>${marker.caption}</h5>
          `;

          div.appendChild(button);

          return div;
        },
      }))
    )
  );

  contacts$ = this.contactsFacade.contacts$.pipe(
    filter(contacts => contacts !== undefined)
  );

  isLoading$ = this.contactsFacade.isLoading$;

  isMarkersLoading$ = this.contactsFacade.isLoadingMarkers$;

  constructor(
    private contactsFacade: ContactsFacade,
    private dialog: MatDialog
  ) {}

  ngOnInit() {
    this.contactsFacade.dispatchGetContacts();
    this.contactsFacade.dispatchGetMarkers();
  }

  contactsEdited({ data, newRecord }: EditHtmlEvent): void {
    if (newRecord) {
      this.contactsFacade.dispatchCreateContacts(data);
    } else {
      this.contactsFacade.dispatchEditContacts(data);
    }
  }

  removeMarker(marker: MapMarkerResponse) {
    const dialog = this.dialog.open(ConfirmDialogComponent, {
      ...COMMON_DIALOG_CONFIG,
      data: {
        message: `Ви дійсно хочете видалити точку ${marker.caption}?`,
      } as ConfirmDialogData,
    });

    dialog
      .afterClosed()
      .pipe(
        take(1),
        untilDestroyed(this),
        filter(confirm => confirm)
      )
      .subscribe(() => this.contactsFacade.dispatchRemoveMarker(marker));
  }

  addMarker({ latlng }: LeafletMouseEvent) {
    const dialog = this.dialog.open(CreateMarkerDialogComponent, {
      ...COMMON_DIALOG_CONFIG,
    });

    dialog
      .afterClosed()
      .pipe(
        take(1),
        untilDestroyed(this),
        filter(data => data)
      )
      .subscribe((formData: CreateMarkerDialogForm) => {
        this.contactsFacade.dispatchAddMarker({
          lat: latlng.lat,
          long: latlng.lng,
          caption: formData.markerName,
        });
      });
  }

  ngOnDestroy() {
    this.markersListeners.forEach(({ listener, element, type }) =>
      element.removeEventListener(type, listener)
    );
  }

  private createRemoveMarkerButton(
    marker: MapMarkerResponse
  ): HTMLButtonElement {
    const listener = this.removeMarker.bind(this, marker);
    const button = document.createElement('button');
    const type = 'click';

    button.innerText = 'Видалити точку';

    button.addEventListener(type, listener);

    this.markersListeners.push({
      element: button,
      listener,
      type,
    });

    return button;
  }
}
