import { CommonModule, NgOptimizedImage } from '@angular/common';
import { Component, OnDestroy, ViewChild } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import {
  AngularEditorConfig,
  AngularEditorModule,
} from '@kolkov/angular-editor';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { UploadFileComponent, WrapperComponent } from '@shared/components';
import { ImageCropperDialogComponent } from '@shared/components/dialogs/image-cropper-dialog/image-cropper-dialog.component';
import { ImageCropperDialogData } from '@shared/components/dialogs/image-cropper-dialog/image-cropper-dialog.interface';
import { UploadFileSelected } from '@shared/components/upload-file';
import { COMMON_DIALOG_CONFIG } from '@shared/constants';
import { IImageCroppedEvent } from '@shared/interfaces';
import { FormDataUtil } from '@shared/utils';
import { ImageCropperModule } from 'ngx-image-cropper';
import { filter, take } from 'rxjs';
import { TEXT_EDITOR_CONFIG } from '../../../../constants/editor.constant';
import { AdminNewsFacade } from '../../state';

const EMPTY_FILE_FORM_CONTROL_KEY = 'null';

@Component({
  selector: 'rdn-state-processing',
  standalone: true,
  imports: [
    CommonModule,
    WrapperComponent,
    ImageCropperModule,
    MatButtonModule,
    AngularEditorModule,
    FormsModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    MatDialogModule,
    NgOptimizedImage,
    MatIconModule,
    UploadFileComponent,
  ],
  templateUrl: './news-processing.component.html',
  styleUrls: ['./news-processing.component.scss'],
})
@UntilDestroy()
export class NewsProcessingComponent implements OnDestroy {
  readonly isLoadingNews$ = this.adminNewsFacade.isLoadingNews$;

  readonly news$ = this.adminNewsFacade.news$;

  readonly form: FormGroup;

  readonly titleFC = new FormControl('', [Validators.required]);

  readonly editorFC = new FormControl('');

  readonly fileFC = new FormControl<
    Blob | typeof EMPTY_FILE_FORM_CONTROL_KEY | null
  >(EMPTY_FILE_FORM_CONTROL_KEY);

  readonly config: AngularEditorConfig = {
    ...TEXT_EDITOR_CONFIG,
    placeholder: 'Додати текст новини...',
  };

  newsId: string;

  selectedImage: string;

  @ViewChild(UploadFileComponent) uploadFileComponent: UploadFileComponent;

  constructor(
    private route: ActivatedRoute,
    private sanitizer: DomSanitizer,
    private fb: FormBuilder,
    private dialog: MatDialog,
    private adminNewsFacade: AdminNewsFacade
  ) {
    this.newsId = route.snapshot.params['newsId'];

    this.form = fb.group({
      html: this.editorFC,
      title: this.titleFC,
      file: this.fileFC,
    });

    if (this.newsId) {
      this.getNewsToEdit();
    }
  }

  selectedFile(fileSelectedEvent: UploadFileSelected): void {
    const dialog = this.dialog.open<
      ImageCropperDialogComponent,
      ImageCropperDialogData
    >(ImageCropperDialogComponent, {
      ...COMMON_DIALOG_CONFIG,
      data: {
        imageChangedEvent: fileSelectedEvent.originalEvent,
      },
    });

    dialog
      .afterClosed()
      .pipe(
        take(1),
        untilDestroyed(this),
        filter(event => !!event)
      )
      .subscribe((croppedEvent: IImageCroppedEvent) => {
        this.uploadFileComponent.reset();
        const file = new File([croppedEvent.blob], croppedEvent.fileName, {
          type: 'image/jpeg',
        });
        this.fileFC.setValue(file);
        this.selectedImage = this.sanitizer.bypassSecurityTrustUrl(
          croppedEvent.objectUrl
        ) as string;
      });
  }

  submitForm(): void {
    if (this.form.valid) {
      const formData = FormDataUtil.createFormDataFromObject(
        this.form.getRawValue()
      );

      if (this.newsId) {
        this.adminNewsFacade.dispatchUpdateNews(this.newsId, formData);
      } else {
        this.adminNewsFacade.dispatchCreateNews(formData);
      }
    }
  }

  resetImage(): void {
    this.fileFC.setValue(EMPTY_FILE_FORM_CONTROL_KEY);
    this.selectedImage = null;
  }

  private getNewsToEdit(): void {
    this.news$
      .pipe(
        filter(news => !!news),
        take(1),
        untilDestroyed(this)
      )
      .subscribe(({ title, html, file }) => {
        this.titleFC.setValue(title);
        this.editorFC.setValue(html);
        this.selectedImage = file?.url;
      });

    this.adminNewsFacade.dispatchGetNews(this.newsId);
  }

  ngOnDestroy() {
    this.adminNewsFacade.dispatchResetState();
  }
}
