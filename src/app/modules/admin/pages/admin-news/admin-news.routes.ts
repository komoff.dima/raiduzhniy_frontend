import { Route, Routes } from '@angular/router';
import { AdminNewsComponent } from './admin-news.component';
import { NewsProcessingComponent } from './pages/news-processing/news-processing.component';

export const ADMIN_NEWS_SUB_ROUTES: Routes = [
  {
    path: '',
    component: AdminNewsComponent,
    data: {
      label: 'Адміністрування',
    },
  },
];

export const ADMIN_NEWS_ROUTES: Route[] = [
  ...ADMIN_NEWS_SUB_ROUTES,
  {
    path: 'create',
    component: NewsProcessingComponent,
    data: {
      isEditMode: false,
    },
  },
  {
    path: ':newsId',
    component: NewsProcessingComponent,
    data: {
      isEditMode: true,
    },
  },
];
