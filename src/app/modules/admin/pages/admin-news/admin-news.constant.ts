import { TableAction, TableSettings } from '@shared/components/table';
import { DateCellComponent } from '@shared/components/table-cells';
import { News, OrderDirection, TableQueryParams } from '@shared/interfaces';

export const DEFAULT_QUERY_PARAMS: TableQueryParams = {
  orderBy: 'createdAt',
  orderDirection: OrderDirection.Desc,
  pageSize: 10,
  pageNumber: 1,
};

export const NEWS_TABLE_SETTINGS: TableSettings<News> = {
  queryParamsStorageKey: 'news-state',
  initialQueryParams: {
    orderBy: 'createdAt',
    orderDirection: OrderDirection.Desc,
    pageSize: 10,
    pageNumber: 1,
  },
  actions: {
    [TableAction.Delete]: {},
    [TableAction.Edit]: {},
  },
  columns: [
    {
      name: 'createdAt',
      label: 'Дата створення',
      sortHeader: true,
      component: {
        class: DateCellComponent,
      },
    },
    {
      name: 'editedAt',
      label: 'Дата редагування',
      sortHeader: true,
      component: {
        class: DateCellComponent,
      },
    },
    {
      name: 'title',
      label: 'Назва',
      sortHeader: true,
    },
  ],
};
