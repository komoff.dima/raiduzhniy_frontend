import { EnvironmentProviders, Provider } from '@angular/core';
import { provideEffects } from '@ngrx/effects';
import { provideState } from '@ngrx/store';
import { NewsService } from '@shared/services';
import { AdminNewsEffects } from './admin-news.effects';
import { AdminNewsFacade } from './admin-news.facade';
import { adminNewsReducer } from './admin-news.reducer';

export const AdminNewsStateProviders: (Provider | EnvironmentProviders)[] = [
  provideEffects(AdminNewsEffects),
  provideState({ name: 'adminNews', reducer: adminNewsReducer }),
  NewsService,
  AdminNewsFacade,
];
