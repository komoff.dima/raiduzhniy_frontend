import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { Router, RouterLink } from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { TableComponent, WrapperComponent } from '@shared/components';
import { ConfirmDialogComponent } from '@shared/components/dialogs';
import { ConfirmDialogData } from '@shared/components/dialogs/confirm-dialog/confirm-dialog.interface';
import { TABLE_SETTINGS } from '@shared/components/table';
import { COMMON_DIALOG_CONFIG } from '@shared/constants/dialog.constant';
import { News, TableQueryParams } from '@shared/interfaces';
import { filter, take } from 'rxjs';
import { NEWS_TABLE_SETTINGS } from './admin-news.constant';
import { AdminNewsFacade } from './state';

@Component({
  selector: 'rdn-admin-state',
  standalone: true,
  imports: [
    CommonModule,
    WrapperComponent,
    MatCardModule,
    MatButtonModule,
    RouterLink,
    MatDialogModule,
    TableComponent,
  ],
  templateUrl: './admin-news.component.html',
  styleUrls: ['./admin-news.component.scss'],
  providers: [
    {
      provide: TABLE_SETTINGS,
      useValue: NEWS_TABLE_SETTINGS,
    },
  ],
})
@UntilDestroy()
export class AdminNewsComponent {
  isLoading$ = this.newsFacade.isLoadingNewsElements$;
  newsElements$ = this.newsFacade.newsElements$;

  constructor(
    private newsFacade: AdminNewsFacade,
    private dialog: MatDialog,
    private router: Router
  ) {}

  editNews(news: News): void {
    this.router.navigate(['admin', 'news', news.id]);
  }

  fetchNews(queryParams: TableQueryParams): void {
    this.newsFacade.dispatchGetNewsElements(queryParams);
  }

  deleteNews(news: News): void {
    const dialog = this.dialog.open(ConfirmDialogComponent, {
      ...COMMON_DIALOG_CONFIG,
      data: {
        message: `Ви дійсно хочете видалити новину ${news.title}?`,
      } as ConfirmDialogData,
    });

    dialog
      .afterClosed()
      .pipe(
        take(1),
        untilDestroyed(this),
        filter(res => !!res)
      )
      .subscribe(() => this.newsFacade.dispatchDeleteNews(news.id));
  }
}
