export interface CreateDocumentFormData {
  approvedAt: string;
  description?: string;
  files: File[];
  name: string;
  type: string;
}

export type EditDocumentFormData = Omit<CreateDocumentFormData, 'files'> & {
  filesTyDelete?: string[];
  files?: File[];
};
