import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { TableComponent, WrapperComponent } from '@shared/components';
import { ConfirmDialogComponent } from '@shared/components/dialogs';
import { ConfirmDialogData } from '@shared/components/dialogs/confirm-dialog/confirm-dialog.interface';
import { TABLE_SETTINGS } from '@shared/components/table';
import { COMMON_DIALOG_CONFIG } from '@shared/constants/dialog.constant';
import { IDocument, TableQueryParams } from '@shared/interfaces';
import { filter, take } from 'rxjs';
import { ADMIN_DOCUMENTS_TABLE_SETTINGS } from './admin-documents-table.constant';
import {
  CreateDocumentFormData,
  EditDocumentFormData,
} from './admin-documents.interface';
import { DocumentProcessingDialogComponent } from './components/document-processing-dialog/document-processing-dialog.component';
import { AdminDocumentsFacade } from './state';

@Component({
  selector: 'rdn-admin-documents',
  standalone: true,
  imports: [
    CommonModule,
    MatButtonModule,
    MatCardModule,
    WrapperComponent,
    MatDialogModule,
    TableComponent,
  ],
  templateUrl: './admin-documents.component.html',
  styleUrls: ['./admin-documents.component.scss'],
  providers: [
    {
      provide: TABLE_SETTINGS,
      useValue: ADMIN_DOCUMENTS_TABLE_SETTINGS,
    },
  ],
})
@UntilDestroy()
export class AdminDocumentsComponent {
  isLoading$ = this.adminDocumentsFacade.documentsIsLoading$;
  documents$ = this.adminDocumentsFacade.documents$;

  constructor(
    private adminDocumentsFacade: AdminDocumentsFacade,
    private dialog: MatDialog
  ) {}

  createDocument(): void {
    this.processDocument();
  }

  editDocument(document: IDocument): void {
    this.processDocument(document);
  }

  deleteDocument(document: IDocument) {
    const dialog = this.dialog.open(ConfirmDialogComponent, {
      ...COMMON_DIALOG_CONFIG,
      data: {
        message: `Ви дійсно хочете видалити документ ${document.name}?`,
      } as ConfirmDialogData,
    });

    dialog
      .afterClosed()
      .pipe(
        take(1),
        untilDestroyed(this),
        filter(res => !!res)
      )
      .subscribe(() =>
        this.adminDocumentsFacade.dispatchDeleteDocument(document.id)
      );
  }

  dispatchData(queryParams: TableQueryParams): void {
    this.adminDocumentsFacade.dispatchGetDocuments(queryParams);
  }

  private processDocument(document?: IDocument): void {
    const dialog = this.dialog.open(DocumentProcessingDialogComponent, {
      ...COMMON_DIALOG_CONFIG,
      data: { document },
    });

    dialog
      .afterClosed()
      .pipe(
        take(1),
        untilDestroyed(this),
        filter(res => !!res)
      )
      .subscribe(
        (documentData: CreateDocumentFormData | EditDocumentFormData) => {
          if (!document) {
            this.adminDocumentsFacade.dispatchCreateDocument(
              documentData as CreateDocumentFormData
            );
          } else {
            this.adminDocumentsFacade.dispatchEditDocument(
              document.id,
              documentData
            );
          }
        }
      );
  }
}
