import { TableAction, TableSettings } from '@shared/components/table';
import { DateCellComponent } from '@shared/components/table-cells';
import { DocumentTypeDto, IDocument, OrderDirection } from '@shared/interfaces';
import { FilesCellComponent } from './components/files-cell/files-cell.component';

export const ADMIN_DOCUMENTS_TABLE_SETTINGS: TableSettings<IDocument> = {
  queryParamsStorageKey: 'document-state',
  initialQueryParams: {
    orderBy: 'createdAt',
    orderDirection: OrderDirection.Desc,
    pageSize: 10,
    pageNumber: 1,
  },
  actions: {
    [TableAction.Delete]: {},
    [TableAction.Edit]: {},
  },
  columns: [
    {
      name: 'createdAt',
      label: 'Дата створення',
      sortHeader: true,
      component: {
        class: DateCellComponent,
      },
    },
    {
      name: 'editedAt',
      label: 'Дата редагування',
      sortHeader: true,
      component: {
        class: DateCellComponent,
      },
    },
    {
      name: 'approvedAt',
      label: 'Дата затвердження',
      sortHeader: true,
      component: {
        class: DateCellComponent,
      },
    },
    {
      name: 'name',
      label: 'Назва',
      sortHeader: true,
    },
    {
      name: 'description',
      label: 'Опис',
      sortHeader: true,
    },
    {
      name: 'type',
      label: 'Тип документу',
      sortHeader: true,
      valueAdapter: (value: DocumentTypeDto) => value.name,
    },
    {
      name: 'files',
      label: 'Файли',
      valueAdapter: (_, document) => `${document.files.length}`,
      component: {
        class: FilesCellComponent,
      },
    },
  ],
};
