import { Routes } from '@angular/router';
import { forRolesGuard } from '@core/guards/for-roles.guard';
import { UserRole } from '@shared/enums';
import { AdminDocumentsComponent } from './admin-documents.component';
import { DocumentTypesComponent } from './pages/document-types/document-types.component';

export const ADMIN_DOCUMENTS_SUB_ROUTES: Routes = [
  {
    path: '',
    component: AdminDocumentsComponent,
    data: {
      label: 'Адміністрування',
    },
  },
  {
    path: 'types',
    component: DocumentTypesComponent,
    canActivate: [forRolesGuard],
    data: {
      label: 'Типи документів',
      forRoles: [UserRole.Superadmin],
    },
  },
];

export const ADMIN_DOCUMENTS_ROUTES: Routes = [...ADMIN_DOCUMENTS_SUB_ROUTES];
