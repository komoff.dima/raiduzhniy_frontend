import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FilesComponent } from '@shared/components';
import { TableCell } from '@shared/components/table';
import { FileDto, IDocument } from '@shared/interfaces';

@Component({
  standalone: true,
  imports: [CommonModule, FilesComponent],
  template: ` <rdn-files [files]="row.files"></rdn-files>`,
})
export class FilesCellComponent implements TableCell<IDocument> {
  @Input() row: IDocument;
  @Input() value: FileDto;

  trackBy(index: number, file: FileDto): string {
    return file.storagePath;
  }
}
