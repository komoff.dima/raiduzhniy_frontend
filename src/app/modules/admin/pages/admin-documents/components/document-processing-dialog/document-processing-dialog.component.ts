import { CommonModule } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatChipsModule } from '@angular/material/chips';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import {
  MAT_DIALOG_DATA,
  MatDialogModule,
  MatDialogRef,
} from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { ORGANIZATION_FOUNDING_DATE } from '@shared/constants';
import { UploadFileDirective } from '@shared/directives';
import { FileDto, IDocument, IRecord } from '@shared/interfaces';
import { BehaviorSubject } from 'rxjs';
import { AdminDocumentTypesFacade } from '../../pages/document-types/state';

export interface DocumentProcessingDialogData {
  document?: IDocument;
}

@Component({
  selector: 'rdn-document-processing-dialog',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    MatButtonModule,
    MatDialogModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatDatepickerModule,
    UploadFileDirective,
    MatChipsModule,
    MatNativeDateModule,
  ],
  templateUrl: './document-processing-dialog.component.html',
  styleUrls: ['./document-processing-dialog.component.scss'],
})
@UntilDestroy()
export class DocumentProcessingDialogComponent implements OnInit {
  existingFiles$ = new BehaviorSubject<FileDto[]>([]);

  maxDate = new Date();
  minDate = ORGANIZATION_FOUNDING_DATE;
  readonly form: FormGroup;

  readonly nameFC = new FormControl<string>(this.data.document?.name || '', [
    Validators.required,
  ]);

  readonly descriptionFC = new FormControl<string>(
    this.data.document?.description || ''
  );

  readonly documentTypeFC = new FormControl<string>(
    this.data.document?.type?.id || '',
    [Validators.required]
  );

  readonly approvedAtFC = new FormControl<Date>(
    this.data.document?.approvedAt
      ? new Date(this.data.document?.approvedAt)
      : null,
    [Validators.required]
  );

  readonly filesFC = new FormControl<File[]>(null, [Validators.required]);

  readonly filesToDeleteFC = new FormControl<string[]>(null);

  readonly isEditMode: boolean;

  readonly documentTypes$ = this.documentTypesFacade.documentTypes$;
  constructor(
    private documentTypesFacade: AdminDocumentTypesFacade,
    fb: FormBuilder,
    public dialogRef: MatDialogRef<DocumentProcessingDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data?: DocumentProcessingDialogData
  ) {
    this.isEditMode = !!data?.document;

    this.existingFiles$.next(data.document?.files);

    this.form = fb.group({
      name: this.nameFC,
      description: this.descriptionFC,
      type: this.documentTypeFC,
      approvedAt: this.approvedAtFC,
      files: this.filesFC,
    });

    if (this.isEditMode) {
      this.filesFC.removeValidators([Validators.required]);

      this.form.addControl('filesToDelete', this.filesToDeleteFC);
    }
  }

  ngOnInit() {
    this.documentTypesFacade.dispatchGetDocumentTypes();

    this.documentTypesFacade.documentTypesIsLoading$
      .pipe(untilDestroyed(this))
      .subscribe(isLoading => {
        if (isLoading) {
          this.form.get('type').disable();
        } else {
          this.form.get('type').enable();
        }
      });
  }

  processDocument(): void {
    if (this.form.valid) {
      const formValues = this.form.getRawValue();

      this.dialogRef.close({
        ...formValues,
        approvedAt: formValues.approvedAt.toISOString(),
      });
    }
  }

  trackById<T extends IRecord>(index: number, item: T): string {
    return item.id;
  }

  trackByStoragePath(index: number, file: FileDto): string {
    return file.storagePath;
  }

  removeFile(file: FileDto, files: FileDto[]): void {
    this.filesToDeleteFC.setValue([
      ...(this.filesToDeleteFC.value || []),
      file.storagePath,
    ]);

    const existingFiles = files.filter(
      existedFile => existedFile.storagePath !== file.storagePath
    );

    this.existingFiles$.next(existingFiles);

    if (!existingFiles.length) {
      this.filesFC.addValidators([Validators.required]);
      this.filesFC.updateValueAndValidity();
    }
  }

  saveFile(link: string) {
    window.open(link, '_blank');
  }
}
