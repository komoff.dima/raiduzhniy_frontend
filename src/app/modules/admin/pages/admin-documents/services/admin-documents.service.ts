import { Injectable } from '@angular/core';
import { ApiService } from '@core/services';
import { DefaultResponse } from '@shared/interfaces';
import { DocumentsService } from '@shared/services';
import { Observable } from 'rxjs';

@Injectable()
export class AdminDocumentsService extends DocumentsService {
  constructor(apiService: ApiService) {
    super(apiService);
  }

  createDocument(formData: FormData): Observable<unknown> {
    return this.apiService.post(`documents/create`, formData);
  }

  editDocument(id: string, formData: FormData): Observable<unknown> {
    return this.apiService.put(`documents/${id}/edit`, formData);
  }

  deleteDocument(id: string): Observable<DefaultResponse> {
    return this.apiService.delete(`documents/${id}/delete`);
  }
}
