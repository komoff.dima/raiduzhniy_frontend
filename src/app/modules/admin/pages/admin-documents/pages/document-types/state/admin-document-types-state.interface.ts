import { TableData, TableQueryParams } from '@shared/interfaces';
import { DocumentType } from '@shared/interfaces/document-types.interface';

export interface AdminDocumentTypesStateInterface {
  documentTypes: TableData<DocumentType>;
  documentTypesIsLoading: boolean;
  documentTypesError: string;
  queryParams: TableQueryParams;
}
