import { createReducer, on } from '@ngrx/store';
import { AdminDocumentTypesStateInterface } from './admin-document-types-state.interface';
import * as DocumentTypesActions from './admin-document-types.actions';

export const initialState: AdminDocumentTypesStateInterface = {
  documentTypes: undefined,
  documentTypesIsLoading: false,
  documentTypesError: undefined,
  queryParams: undefined,
};

export const adminDocumentTypesReducer =
  createReducer<AdminDocumentTypesStateInterface>(
    initialState,
    on(DocumentTypesActions.getDocumentTypes, (state, action) => ({
      ...state,
      documentTypesIsLoading: true,
      queryParams: action.queryParams,
    })),
    on(DocumentTypesActions.getDocumentTypesSuccess, (state, action) => ({
      ...state,
      documentTypesIsLoading: false,
      documentTypes: action.documentTypes,
    })),
    on(DocumentTypesActions.getDocumentTypesFailure, (state, action) => ({
      ...state,
      documentTypesIsLoading: false,
      documentTypeError: action.error,
    })),
    on(DocumentTypesActions.deleteDocumentType, state => ({
      ...state,
      documentTypesIsLoading: true,
    })),
    on(DocumentTypesActions.createDocumentType, state => ({
      ...state,
      documentTypesIsLoading: true,
    })),
    on(DocumentTypesActions.editDocumentType, state => ({
      ...state,
      documentTypesIsLoading: true,
    }))
  );
