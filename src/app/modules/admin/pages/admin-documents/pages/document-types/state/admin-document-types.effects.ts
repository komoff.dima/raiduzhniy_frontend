import { inject, Injectable } from '@angular/core';
import { AppStateInterface } from '@core/types/app-state.interface';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { catchError, map, mergeMap, of, withLatestFrom } from 'rxjs';
import { AdminDocumentTypesService } from '../services/admin-document-types.service';
import * as DocumentTypesActions from './admin-document-types.actions';
import { selectDocumentTypesQueryParams } from './admin-document-types.selector';

@Injectable()
export class AdminDocumentTypesEffects {
  private adminDocumentTypesService = inject(AdminDocumentTypesService);
  private actions$ = inject(Actions);
  private store: Store<AppStateInterface> = inject(Store);

  getDocumentTypes$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(DocumentTypesActions.getDocumentTypes),
      mergeMap(({ queryParams }) => {
        return this.adminDocumentTypesService
          .getDocumentTypes(queryParams)
          .pipe(
            map(response =>
              DocumentTypesActions.getDocumentTypesSuccess({
                documentTypes: response,
              })
            ),
            catchError(error =>
              of(
                DocumentTypesActions.getDocumentTypesFailure({
                  error: error.message,
                })
              )
            )
          );
      })
    );
  });

  createDocumentType$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(DocumentTypesActions.createDocumentType),
      mergeMap(({ documentTypeDto }) => {
        return this.adminDocumentTypesService
          .createDocumentType(documentTypeDto)
          .pipe(
            withLatestFrom(this.store.select(selectDocumentTypesQueryParams)),
            map(([_, queryParams]) =>
              DocumentTypesActions.getDocumentTypes({ queryParams })
            )
          );
      })
    );
  });

  editDocumentType$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(DocumentTypesActions.editDocumentType),
      mergeMap(({ documentTypeDto, id }) => {
        return this.adminDocumentTypesService
          .editDocumentType(id, documentTypeDto)
          .pipe(
            withLatestFrom(this.store.select(selectDocumentTypesQueryParams)),
            map(([_, queryParams]) =>
              DocumentTypesActions.getDocumentTypes({ queryParams })
            )
          );
      })
    );
  });

  deleteDocumentType$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(DocumentTypesActions.deleteDocumentType),
      mergeMap(({ id }) => {
        return this.adminDocumentTypesService.deleteDocumentType(id).pipe(
          withLatestFrom(this.store.select(selectDocumentTypesQueryParams)),
          map(([_, queryParams]) =>
            DocumentTypesActions.getDocumentTypes({ queryParams })
          )
        );
      })
    );
  });
}
