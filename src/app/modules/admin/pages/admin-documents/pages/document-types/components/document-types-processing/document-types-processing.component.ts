import { CommonModule } from '@angular/common';
import { Component, Inject } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import {
  MAT_DIALOG_DATA,
  MatDialogModule,
  MatDialogRef,
} from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { DocumentType } from '@shared/interfaces';
import { UniqueAsyncValidator } from '@shared/validators';
import { AdminDocumentTypesService } from '../../services/admin-document-types.service';

export interface DocumentTypesProcessingDialogData {
  documentType?: DocumentType;
}

@Component({
  selector: 'rdn-document-types-processing',
  standalone: true,
  imports: [
    CommonModule,
    MatButtonModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    MatIconModule,
  ],
  templateUrl: './document-types-processing.component.html',
  styleUrls: ['./document-types-processing.component.scss'],
})
export class DocumentTypesProcessingComponent {
  form: FormGroup;

  nameFC = new FormControl<string>(this.data.documentType?.name || '', [
    Validators.required,
  ]);

  isEditMode: boolean;

  constructor(
    adminDocumentTypesService: AdminDocumentTypesService,
    fb: FormBuilder,
    public dialogRef: MatDialogRef<DocumentTypesProcessingComponent>,
    @Inject(MAT_DIALOG_DATA) public data?: DocumentTypesProcessingDialogData
  ) {
    this.isEditMode = !!data?.documentType;

    if (!this.isEditMode) {
      this.nameFC.addAsyncValidators(
        UniqueAsyncValidator.createValidator('name', adminDocumentTypesService)
      );
    }

    this.form = fb.group({
      name: this.nameFC,
    });
  }

  processDocumentType() {
    if (this.form.valid && this.form.status !== 'PENDING') {
      this.dialogRef.close(this.form.getRawValue());
    }
  }
}
