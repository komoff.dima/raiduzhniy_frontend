import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { TableComponent, WrapperComponent } from '@shared/components';
import { ConfirmDialogComponent } from '@shared/components/dialogs';
import { ConfirmDialogData } from '@shared/components/dialogs/confirm-dialog/confirm-dialog.interface';
import { TABLE_SETTINGS } from '@shared/components/table';
import { COMMON_DIALOG_CONFIG } from '@shared/constants';
import {
  DocumentType,
  DocumentTypeDto,
  TableQueryParams,
} from '@shared/interfaces';
import { filter, take } from 'rxjs';
import {
  DocumentTypesProcessingComponent,
  DocumentTypesProcessingDialogData,
} from './components/document-types-processing/document-types-processing.component';
import { DOCUMENT_TYPES_TABLE_SETTINGS } from './document-types.constant';
import { AdminDocumentTypesFacade } from './state';

@Component({
  selector: 'rdn-document-types',
  standalone: true,
  imports: [
    CommonModule,
    MatButtonModule,
    MatCardModule,
    WrapperComponent,
    MatDialogModule,
    TableComponent,
  ],
  templateUrl: './document-types.component.html',
  styleUrls: ['./document-types.component.scss'],
  providers: [
    {
      provide: TABLE_SETTINGS,
      useValue: DOCUMENT_TYPES_TABLE_SETTINGS,
    },
  ],
})
@UntilDestroy()
export class DocumentTypesComponent {
  isLoading$ = this.adminDocumentsFacade.documentTypesIsLoading$;
  documentTypes$ = this.adminDocumentsFacade.documentTypes$;

  constructor(
    private adminDocumentsFacade: AdminDocumentTypesFacade,
    private dialog: MatDialog
  ) {}

  createDocumentType(): void {
    this.processDocumentType();
  }

  editDocumentType(documentType: DocumentType): void {
    this.processDocumentType(documentType);
  }

  deleteDocumentType(documentType: DocumentType) {
    const dialog = this.dialog.open(ConfirmDialogComponent, {
      ...COMMON_DIALOG_CONFIG,
      data: {
        message: `Ви дійсно хочете видалити тип документу ${documentType.name}?`,
      } as ConfirmDialogData,
    });

    dialog
      .afterClosed()
      .pipe(
        take(1),
        untilDestroyed(this),
        filter(res => !!res)
      )
      .subscribe(() =>
        this.adminDocumentsFacade.dispatchDeleteDocumentType(documentType.id)
      );
  }

  fetchData(queryParams: TableQueryParams): void {
    this.adminDocumentsFacade.dispatchGetDocumentTypes(queryParams);
  }

  private processDocumentType(documentType?: DocumentType): void {
    const dialog = this.dialog.open(DocumentTypesProcessingComponent, {
      ...COMMON_DIALOG_CONFIG,
      data: { documentType } as DocumentTypesProcessingDialogData,
    });

    dialog
      .afterClosed()
      .pipe(
        take(1),
        untilDestroyed(this),
        filter(res => !!res)
      )
      .subscribe((documentTypeDto: DocumentTypeDto) => {
        if (!documentType) {
          this.adminDocumentsFacade.dispatchCreateDocumentType(documentTypeDto);
        } else {
          this.adminDocumentsFacade.dispatchEditDocumentType(
            documentType.id,
            documentTypeDto
          );
        }
      });
  }
}
