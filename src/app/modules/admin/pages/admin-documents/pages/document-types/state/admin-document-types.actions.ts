import { createAction, props } from '@ngrx/store';
import {
  DocumentType,
  DocumentTypeDto,
  TableData,
  TableQueryParams,
} from '@shared/interfaces';

const IDENTIFIER = 'Admin Document Types';

export const getDocumentTypes = createAction(
  `[${IDENTIFIER}] Get Document Types`,
  props<{ queryParams?: TableQueryParams }>()
);

export const getDocumentTypesSuccess = createAction(
  `[${IDENTIFIER}] Get Document Types Success`,
  props<{ documentTypes: TableData<DocumentType> }>()
);

export const getDocumentTypesFailure = createAction(
  `[${IDENTIFIER}] Get Document Types Failure`,
  props<{ error: string }>()
);

export const deleteDocumentType = createAction(
  `[${IDENTIFIER}] Delete Document Type`,
  props<{ id: string }>()
);

export const createDocumentType = createAction(
  `[${IDENTIFIER}] Create Document Type`,
  props<{ documentTypeDto: DocumentTypeDto }>()
);

export const editDocumentType = createAction(
  `[${IDENTIFIER}] Edit Document Type`,
  props<{ id: string; documentTypeDto: DocumentTypeDto }>()
);
