import { Injectable } from '@angular/core';
import { ApiService } from '@core/services';
import {
  DefaultResponse,
  DocumentType,
  DocumentTypeDto,
  UniqueValidator,
} from '@shared/interfaces';
import { DocumentTypesService } from '@shared/services';
import { Observable } from 'rxjs';

@Injectable()
export class AdminDocumentTypesService
  extends DocumentTypesService
  implements UniqueValidator
{
  constructor(apiService: ApiService) {
    super(apiService);
  }

  createDocumentType(
    documentTypeDto: DocumentTypeDto
  ): Observable<DocumentType> {
    return this.apiService.post(`document-types/create`, documentTypeDto);
  }

  editDocumentType(
    id: string,
    documentTypeDto: DocumentTypeDto
  ): Observable<DefaultResponse> {
    return this.apiService.put(`document-types/${id}/edit`, documentTypeDto);
  }

  deleteDocumentType(id: string): Observable<DefaultResponse> {
    return this.apiService.delete(`document-types/${id}/delete`);
  }

  validateUnique(validationDto: DocumentTypeDto): Observable<DefaultResponse> {
    return this.apiService.post(`document-types/validate-name`, validationDto);
  }
}
