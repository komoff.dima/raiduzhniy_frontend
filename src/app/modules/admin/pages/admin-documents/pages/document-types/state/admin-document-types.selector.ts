import { AppStateInterface } from '@core/types/app-state.interface';
import { createSelector } from '@ngrx/store';

export const selectFeature = (state: AppStateInterface) =>
  state.adminDocumentTypes;

export const selectDocumentTypes = createSelector(
  selectFeature,
  state => state.documentTypes
);

export const selectDocumentTypesIsLoading = createSelector(
  selectFeature,
  state => state.documentTypesIsLoading
);

export const selectDocumentTypesQueryParams = createSelector(
  selectFeature,
  state => state.queryParams
);
