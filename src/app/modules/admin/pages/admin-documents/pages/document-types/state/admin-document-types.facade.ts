import { Injectable } from '@angular/core';
import { AppStateInterface } from '@core/types/app-state.interface';
import { Store } from '@ngrx/store';
import { DocumentTypeDto, TableQueryParams } from '@shared/interfaces';
import * as DocumentTypesActions from './admin-document-types.actions';
import {
  selectDocumentTypes,
  selectDocumentTypesIsLoading,
} from './admin-document-types.selector';

@Injectable()
export class AdminDocumentTypesFacade {
  documentTypesIsLoading$ = this.store.select(selectDocumentTypesIsLoading);
  documentTypes$ = this.store.select(selectDocumentTypes);

  constructor(private store: Store<AppStateInterface>) {}

  dispatchGetDocumentTypes(queryParams?: TableQueryParams): void {
    this.store.dispatch(DocumentTypesActions.getDocumentTypes({ queryParams }));
  }

  dispatchCreateDocumentType(documentTypeDto: DocumentTypeDto): void {
    this.store.dispatch(
      DocumentTypesActions.createDocumentType({ documentTypeDto })
    );
  }

  dispatchEditDocumentType(id: string, documentTypeDto: DocumentTypeDto): void {
    this.store.dispatch(
      DocumentTypesActions.editDocumentType({ id, documentTypeDto })
    );
  }

  dispatchDeleteDocumentType(id: string): void {
    this.store.dispatch(DocumentTypesActions.deleteDocumentType({ id }));
  }
}
