import { TableAction, TableSettings } from '@shared/components/table';
import { DateCellComponent } from '@shared/components/table-cells';
import {
  DocumentType,
  OrderDirection,
  TableQueryParams,
} from '@shared/interfaces';

export const DEFAULT_QUERY_PARAMS: TableQueryParams = {
  orderBy: 'createdAt',
  orderDirection: OrderDirection.Desc,
  pageSize: 10,
  pageNumber: 1,
};

export const DOCUMENT_TYPES_TABLE_SETTINGS: TableSettings<DocumentType> = {
  queryParamsStorageKey: 'document-types-state',
  initialQueryParams: {
    orderBy: 'createdAt',
    orderDirection: OrderDirection.Desc,
    pageSize: 10,
    pageNumber: 1,
  },
  actions: {
    [TableAction.Delete]: {},
    [TableAction.Edit]: {},
  },
  columns: [
    {
      name: 'createdAt',
      label: 'Дата створення',
      sortHeader: true,
      component: {
        class: DateCellComponent,
      },
    },
    {
      name: 'editedAt',
      label: 'Дата редагування',
      sortHeader: true,
      component: {
        class: DateCellComponent,
      },
    },
    {
      name: 'name',
      label: 'Назва',
      sortHeader: true,
    },
  ],
};
