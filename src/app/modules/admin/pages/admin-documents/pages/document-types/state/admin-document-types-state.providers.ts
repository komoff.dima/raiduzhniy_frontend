import { EnvironmentProviders, Provider } from '@angular/core';
import { provideEffects } from '@ngrx/effects';
import { provideState } from '@ngrx/store';
import { AdminDocumentTypesService } from '../services/admin-document-types.service';
import { AdminDocumentTypesEffects } from './admin-document-types.effects';
import { AdminDocumentTypesFacade } from './admin-document-types.facade';
import { adminDocumentTypesReducer } from './admin-document-types.reducer';

export const AdminDocumentTypesStateProviders: (
  | Provider
  | EnvironmentProviders
)[] = [
  provideEffects(AdminDocumentTypesEffects),
  provideState({
    name: 'adminDocumentTypes',
    reducer: adminDocumentTypesReducer,
  }),
  AdminDocumentTypesService,
  AdminDocumentTypesFacade,
];
