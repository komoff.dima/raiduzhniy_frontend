import { EnvironmentProviders, Provider } from '@angular/core';
import { provideEffects } from '@ngrx/effects';
import { provideState } from '@ngrx/store';
import { AdminDocumentsService } from '../services/admin-documents.service';
import { AdminDocumentsEffects } from './admin-documents.effects';
import { AdminDocumentsFacade } from './admin-documents.facade';
import { adminDocumentsReducer } from './admin-documents.reducer';

export const AdminDocumentsStateProviders: (Provider | EnvironmentProviders)[] =
  [
    provideEffects(AdminDocumentsEffects),
    provideState({
      name: 'adminDocuments',
      reducer: adminDocumentsReducer,
    }),
    AdminDocumentsService,
    AdminDocumentsFacade,
  ];
