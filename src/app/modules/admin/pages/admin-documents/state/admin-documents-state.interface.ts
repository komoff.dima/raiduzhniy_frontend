import { IDocument, TableData, TableQueryParams } from '@shared/interfaces';

export interface AdminDocumentsStateInterface {
  documents: TableData<IDocument>;
  documentsIsLoading: boolean;
  documentsError: string;
  queryParams: TableQueryParams;
}
