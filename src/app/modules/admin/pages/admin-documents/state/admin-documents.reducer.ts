import { createReducer, on } from '@ngrx/store';
import { AdminDocumentsStateInterface } from './admin-documents-state.interface';
import * as DocumentsActions from './admin-documents.actions';

export const initialState: AdminDocumentsStateInterface = {
  documents: undefined,
  documentsIsLoading: false,
  documentsError: undefined,
  queryParams: undefined,
};

export const adminDocumentsReducer =
  createReducer<AdminDocumentsStateInterface>(
    initialState,
    on(DocumentsActions.getDocuments, (state, action) => ({
      ...state,
      documentsIsLoading: true,
      queryParams: action.queryParams,
    })),
    on(DocumentsActions.getDocumentsSuccess, (state, action) => ({
      ...state,
      documentsIsLoading: false,
      documents: action.documents,
    })),
    on(DocumentsActions.getDocumentsFailure, (state, action) => ({
      ...state,
      documentsIsLoading: false,
      documentsError: action.error,
    })),
    on(DocumentsActions.deleteDocument, state => ({
      ...state,
      documentsIsLoading: true,
    })),
    on(DocumentsActions.createDocument, state => ({
      ...state,
      documentsIsLoading: true,
    })),
    on(DocumentsActions.editDocument, state => ({
      ...state,
      documentsIsLoading: true,
    }))
  );
