import { AppStateInterface } from '@core/types/app-state.interface';
import { createSelector } from '@ngrx/store';

export const selectFeature = (state: AppStateInterface) => state.adminDocuments;

export const selectDocuments = createSelector(
  selectFeature,
  state => state.documents
);

export const selectDocumentsIsLoading = createSelector(
  selectFeature,
  state => state.documentsIsLoading
);

export const selectDocumentsQueryParams = createSelector(
  selectFeature,
  state => state.queryParams
);
