import { createAction, props } from '@ngrx/store';
import {
  DocumentDto,
  IDocument,
  TableData,
  TableQueryParams,
} from '@shared/interfaces';
import {
  CreateDocumentFormData,
  EditDocumentFormData,
} from '../admin-documents.interface';

const IDENTIFIER = 'Admin Documents';

export const getDocuments = createAction(
  `[${IDENTIFIER}] Get Documents`,
  props<{ queryParams?: TableQueryParams }>()
);

export const getDocumentsSuccess = createAction(
  `[${IDENTIFIER}] Get Document Success`,
  props<{ documents: TableData<IDocument> }>()
);

export const getDocumentsFailure = createAction(
  `[${IDENTIFIER}] Get Document Failure`,
  props<{ error: string }>()
);

export const deleteDocument = createAction(
  `[${IDENTIFIER}] Delete Document`,
  props<{ id: string }>()
);

export const createDocument = createAction(
  `[${IDENTIFIER}] Create Document`,
  props<{ documentData: CreateDocumentFormData }>()
);

export const editDocument = createAction(
  `[${IDENTIFIER}] Edit Document`,
  props<{ id: string; documentData: EditDocumentFormData }>()
);
