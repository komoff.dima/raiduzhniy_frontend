import { inject, Injectable } from '@angular/core';
import { AppStateInterface } from '@core/types/app-state.interface';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { FormDataUtil } from '@shared/utils';
import {
  catchError,
  map,
  mergeMap,
  of,
  OperatorFunction,
  withLatestFrom,
} from 'rxjs';
import { AdminDocumentsService } from '../services/admin-documents.service';
import * as DocumentsActions from './admin-documents.actions';
import { selectDocumentsQueryParams } from './admin-documents.selector';

@Injectable()
export class AdminDocumentsEffects {
  private adminDocumentsService = inject(AdminDocumentsService);
  private actions$ = inject(Actions);
  private store: Store<AppStateInterface> = inject(Store);

  getDocuments$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(DocumentsActions.getDocuments),
      mergeMap(({ queryParams }) => {
        return this.adminDocumentsService.getDocuments(queryParams).pipe(
          map(response =>
            DocumentsActions.getDocumentsSuccess({
              documents: response,
            })
          ),
          catchError(error =>
            of(
              DocumentsActions.getDocumentsFailure({
                error: error.message,
              })
            )
          )
        );
      })
    );
  });

  deleteDocument$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(DocumentsActions.deleteDocument),
      mergeMap(({ id }) => {
        return this.adminDocumentsService.deleteDocument(id).pipe(
          withLatestFrom(this.store.select(selectDocumentsQueryParams)),
          map(([_, queryParams]) =>
            DocumentsActions.getDocuments({ queryParams })
          )
        );
      })
    );
  });

  createDocument$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(DocumentsActions.createDocument),
      mergeMap(({ documentData }) => {
        const formData = FormDataUtil.createFormDataFromObject(documentData);

        return this.adminDocumentsService
          .createDocument(formData)
          .pipe(...this.reloadList);
      })
    );
  });

  editDocument$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(DocumentsActions.editDocument),
      mergeMap(({ documentData, id }) => {
        const formData = FormDataUtil.createFormDataFromObject(documentData);

        return this.adminDocumentsService
          .editDocument(id, formData)
          .pipe(...this.reloadList);
      })
    );
  });

  private get reloadList(): [
    OperatorFunction<any, any>,
    OperatorFunction<any, any>,
  ] {
    return [
      withLatestFrom(this.store.select(selectDocumentsQueryParams)),
      map(([_, queryParams]) => DocumentsActions.getDocuments({ queryParams })),
    ];
  }
}
