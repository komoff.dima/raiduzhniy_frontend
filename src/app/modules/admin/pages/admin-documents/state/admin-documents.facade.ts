import { Injectable } from '@angular/core';
import { AppStateInterface } from '@core/types/app-state.interface';
import { Store } from '@ngrx/store';
import { TableQueryParams } from '@shared/interfaces';
import {
  CreateDocumentFormData,
  EditDocumentFormData,
} from '../admin-documents.interface';
import * as DocumentsActions from './admin-documents.actions';
import {
  selectDocuments,
  selectDocumentsIsLoading,
} from './admin-documents.selector';

@Injectable()
export class AdminDocumentsFacade {
  documentsIsLoading$ = this.store.select(selectDocumentsIsLoading);
  documents$ = this.store.select(selectDocuments);

  constructor(private store: Store<AppStateInterface>) {}

  dispatchGetDocuments(queryParams?: TableQueryParams): void {
    this.store.dispatch(DocumentsActions.getDocuments({ queryParams }));
  }

  dispatchCreateDocument(documentData: CreateDocumentFormData): void {
    this.store.dispatch(DocumentsActions.createDocument({ documentData }));
  }

  dispatchEditDocument(id: string, documentData: EditDocumentFormData): void {
    this.store.dispatch(DocumentsActions.editDocument({ documentData, id }));
  }

  dispatchDeleteDocument(id: string): void {
    this.store.dispatch(DocumentsActions.deleteDocument({ id }));
  }
}
