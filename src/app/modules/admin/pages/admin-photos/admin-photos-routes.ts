import { Routes } from '@angular/router';
import { AdminPhotosComponent } from './admin-photos.component';

export const ADMIN_PHOTOS_SUB_ROUTES: Routes = [
  {
    path: '',
    component: AdminPhotosComponent,
    data: {
      label: 'Адміністрування',
    },
  },
];

export const ADMIN_PHOTOS_ROUTES: Routes = [...ADMIN_PHOTOS_SUB_ROUTES];
