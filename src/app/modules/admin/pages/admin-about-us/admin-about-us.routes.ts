import { Route, Routes } from '@angular/router';
import { AdminAboutUsComponent } from './admin-about-us.component';

export const ADMIN_ABOUT_US_SUB_ROUTES: Routes = [
  {
    path: '',
    component: AdminAboutUsComponent,
    data: {
      label: 'Адміністрування',
    },
  },
];

export const ADMIN_ABOUT_US_ROUTES: Route[] = [...ADMIN_ABOUT_US_SUB_ROUTES];
