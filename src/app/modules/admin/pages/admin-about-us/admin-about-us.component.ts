import { CommonModule } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { UntilDestroy } from '@ngneat/until-destroy';
import { HtmlEditorComponent } from '@shared/components';
import { EditHtmlEvent, Html } from '@shared/interfaces';
import { AboutUsFacade } from '@state/about-us';
import { filter } from 'rxjs';

@Component({
  selector: 'rdn-admin-about-us',
  standalone: true,
  imports: [CommonModule, HtmlEditorComponent],
  providers: [AboutUsFacade],
  templateUrl: './admin-about-us.component.html',
  styleUrls: ['./admin-about-us.component.scss'],
})
@UntilDestroy()
export class AdminAboutUsComponent implements OnInit {
  aboutUs$ = this.aboutUsFacade.aboutUs$.pipe(
    filter(aboutUs => aboutUs !== undefined)
  );

  isLoading$ = this.aboutUsFacade.isLoading$;

  constructor(private aboutUsFacade: AboutUsFacade) {}

  ngOnInit() {
    this.aboutUsFacade.dispatchGetAboutUs();
  }

  edit({ data, newRecord }: EditHtmlEvent): void {
    if (newRecord) {
      this.aboutUsFacade.dispatchCreateAboutUs(data);
    } else {
      this.aboutUsFacade.dispatchEditAboutUs(data);
    }
  }
}
