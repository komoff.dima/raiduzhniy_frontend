import { CommonModule, NgOptimizedImage } from '@angular/common';
import { Component, HostBinding, OnInit } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatTabsModule } from '@angular/material/tabs';
import { MatToolbarModule } from '@angular/material/toolbar';
import { RouterLink, RouterLinkActive, RouterOutlet } from '@angular/router';
import { BreakpointsService } from '@core/services';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { UserMenuComponent } from '@shared/components';
import { RoutesAccordionComponent } from '@shared/components/routes-accordion/routes-accordion.component';
import { RoutesAccordion } from '@shared/components/routes-accordion/routes-accordion.interface';
import { Breakpoints } from '@shared/enums';
import {
  LogoWithoutLabelIconComponent,
  MainLogoComponent,
} from '@shared/svg-icons';
import { map } from 'rxjs';
import { ADMIN_CHILDREN_ROUTES } from './admin.routes';

@Component({
  selector: 'rdn-admin',
  standalone: true,
  imports: [
    CommonModule,
    RouterOutlet,
    MatToolbarModule,
    MatTabsModule,
    RouterLink,
    RouterLinkActive,
    NgOptimizedImage,
    MainLogoComponent,
    LogoWithoutLabelIconComponent,
    UserMenuComponent,
    MatSidenavModule,
    MatIconModule,
    MatButtonModule,
    RoutesAccordionComponent,
  ],
  templateUrl: './admin.component.html',
  styleUrls: ['admin.component.scss'],
})
@UntilDestroy()
export class AdminComponent implements OnInit {
  @HostBinding('class.is-mobile') matches: boolean;

  adminRoutes: RoutesAccordion[] = ADMIN_CHILDREN_ROUTES;

  isMobileView$ = this.breakpointsService
    .observeBreakpoint(Breakpoints.SM)
    .pipe(map(matches => !matches));

  constructor(private breakpointsService: BreakpointsService) {}

  ngOnInit() {
    this.isMobileView$.pipe(untilDestroyed(this)).subscribe(matches => {
      this.matches = matches;
    });
  }
}
