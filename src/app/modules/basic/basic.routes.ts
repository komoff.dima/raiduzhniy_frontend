import { Route } from '@angular/router';
import { provideEffects } from '@ngrx/effects';
import { provideState } from '@ngrx/store';
import { NewsService } from '@shared/services';
import { AboutUsStateProviders } from '@state/about-us/about-us-state.providers';
import { ContactsStateProviders } from '@state/contacts';
import { BasicComponent } from './basic.component';
import { NewsEffects, NewsFacade, newsReducer } from './pages/news/state';
import { ScrollableContainerService } from './services';

export const COMMON_ROUTES: Route[] = [
  {
    path: 'news',
    loadChildren: () =>
      import('./pages/news/news.routes').then(mod => mod.NEWS_ROUTES),
    data: {
      label: 'Новини',
    },
    providers: [
      provideEffects(NewsEffects),
      provideState({ name: 'news', reducer: newsReducer }),
      NewsService,
      NewsFacade,
    ],
  },
  {
    path: 'documents',
    loadChildren: () =>
      import('./pages/documents/documents.routes').then(
        mod => mod.DOCUMENTS_ROUTES
      ),
    data: {
      label: 'Документи',
    },
  },
  // {
  //   path: 'services',
  //   component: ServicesComponent,
  //   data: {
  //     label: 'Послуги',
  //   },
  // },
  {
    path: 'about-us',
    loadChildren: () =>
      import('./pages/about-us/about-us.routes').then(
        mod => mod.ABOUT_US_ROUTES
      ),
    data: {
      label: 'Про нас',
    },
    providers: [...AboutUsStateProviders],
  },
  {
    path: 'contacts',
    loadChildren: () =>
      import('./pages/contacts/contacts.routes').then(
        mod => mod.CONTACTS_ROUTES
      ),
    data: {
      label: 'Контакти',
    },
    providers: [...ContactsStateProviders],
  },
];

export const BASIC_ROUTES: Route[] = [
  {
    path: '',
    component: BasicComponent,
    providers: [ScrollableContainerService],
    children: [
      {
        path: '',
        redirectTo: 'news',
        pathMatch: 'full',
      },
      ...COMMON_ROUTES,
    ],
  },
];
