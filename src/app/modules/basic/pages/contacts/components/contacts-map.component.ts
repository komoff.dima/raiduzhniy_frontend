import { CommonModule } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { UpdatesDependentComponent } from '@shared/abstract-classes';
import { MapComponent } from '@shared/components';
import { MapMarkerResponse } from '@shared/interfaces';
import { ContactsFacade } from '@state/contacts';
import { map } from 'rxjs';

@Component({
  selector: 'rdn-contacts-map',
  template: `
    <rdn-map
      class="contacts__map"
      [markers]="markers$ | async"
      [isLoading]="isMarkersLoading$ | async"></rdn-map>
  `,
  imports: [CommonModule, MapComponent],
  standalone: true,
})
export class ContactsMapComponent
  extends UpdatesDependentComponent
  implements OnInit
{
  isMarkersLoading$ = this.contactsFacade.isLoadingMarkers$;

  markers$ = this.contactsFacade.markers$.pipe(
    map(markers =>
      markers.map(marker => ({
        ...marker,
        content: () => {
          const div = document.createElement('div');
          const anchor = this.createLinkToGoogleMaps(marker);
          div.innerHTML = `
            <h5>${marker.caption}</h5>
          `;

          div.appendChild(anchor);

          return div;
        },
      }))
    )
  );

  constructor(private contactsFacade: ContactsFacade) {
    super('mapmarkers');
  }

  updateState() {
    this.contactsFacade.dispatchGetMarkers();
  }

  private createLinkToGoogleMaps(marker: MapMarkerResponse): HTMLAnchorElement {
    const a = document.createElement('a');
    a.href = `http://www.google.com/maps/place/${marker.lat},${marker.long}`;
    a.target = '_blank';
    a.innerText = 'Дивитися на Google Maps';

    return a;
  }
}
