import { Route } from '@angular/router';
import { ContactsComponent } from './contacts.component';

export const CONTACTS_ROUTES: Route[] = [
  {
    path: '',
    component: ContactsComponent,
  },
];
