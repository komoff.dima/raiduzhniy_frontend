import { Route } from '@angular/router';
import { AboutUsComponent } from './about-us.component';

export const ABOUT_US_ROUTES: Route[] = [
  {
    path: '',
    component: AboutUsComponent,
  },
];
