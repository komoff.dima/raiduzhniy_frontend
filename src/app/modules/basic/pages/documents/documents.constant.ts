import {
  FilterableContentOptions,
  FilterType,
} from '@shared/components/filterable-content';
import { SortingDropdownOptionInput } from '@shared/components/filterable-content/components';
import { OrderDirection } from '@shared/interfaces';
import { DocumentTypesService } from '@shared/services';
import { map, of } from 'rxjs';

export const DOCUMENTS_INITIAL_SORTING_OBJECT: SortingDropdownOptionInput = {
  key: 'createdAt',
  label: 'Пізніше додані',
  direction: OrderDirection.Desc,
};

export const documentFiltersFactory = (
  documentTypesService: DocumentTypesService
): FilterableContentOptions => {
  return {
    filters$: of([
      {
        key: 'name',
        type: FilterType.Search,
        label: 'Назва',
        inputs: {
          label: 'Назва документу',
        },
      },
      {
        type: FilterType.Options,
        label: 'Тип документу',
        key: 'type',
        inputs: {
          options: documentTypesService.getDocumentTypes().pipe(
            map(documentTypes =>
              documentTypes.elements.map(elem => ({
                key: elem.id,
                label: elem.name,
              }))
            )
          ),
        },
      },
      {
        key: 'approvedAt',
        type: FilterType.DateRange,
        label: 'Дата затвердження документу',
      },
    ]),
    sorting$: of([
      {
        key: 'name',
        label: 'За назвою документа',
        direction: OrderDirection.Acs,
      },
      {
        key: 'type',
        label: 'За типом',
        direction: OrderDirection.Acs,
      },
      {
        key: 'approvedAt',
        label: 'Раніше затвердженні',
        direction: OrderDirection.Acs,
      },
      {
        key: 'approvedAt',
        label: 'Пізніше затвердженні',
        direction: OrderDirection.Desc,
      },
      {
        key: 'createdAt',
        label: 'Раніше додані',
        direction: OrderDirection.Acs,
      },
      {
        ...DOCUMENTS_INITIAL_SORTING_OBJECT,
        initial: true,
      },
    ]),
  };
};
