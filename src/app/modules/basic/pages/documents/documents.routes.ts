import { Route } from '@angular/router';
import { DocumentsComponent } from './documents.component';
import { DocumentsStateProviders } from './state/documents-state.providers';

export const DOCUMENTS_ROUTES: Route[] = [
  {
    path: '',
    component: DocumentsComponent,
    providers: [...DocumentsStateProviders],
  },
];
