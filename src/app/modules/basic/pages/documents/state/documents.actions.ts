import { createAction, props } from '@ngrx/store';
import { AdaptedFilters } from '@shared/components/filterable-content';
import { IDocument, SortStatus, TableData } from '@shared/interfaces';

const IDENTIFIER = 'Documents';

export const sortDocuments = createAction(
  `[${IDENTIFIER}] Sort Documents`,
  props<{ sortStatus: SortStatus }>()
);

export const sortFilters = createAction(
  `[${IDENTIFIER}] Set Filters`,
  props<{ filters: AdaptedFilters }>()
);

export const getNextDocumentsPage = createAction(
  `[${IDENTIFIER}] Get Next Documents Page`
);

export const resetState = createAction(`[${IDENTIFIER}] Reset state`);

export const getDocuments = createAction(
  `[${IDENTIFIER}] Get Document`,
  props<{
    pageNumber: number;
    sortStatus?: SortStatus;
    filters?: AdaptedFilters;
  }>()
);

export const getDocumentsSuccess = createAction(
  `[${IDENTIFIER}] Get Document Success`,
  props<{ documents: TableData<IDocument> }>()
);

export const getDocumentsFailure = createAction(
  `[${IDENTIFIER}] Get Document Failure`,
  props<{ error: string }>()
);
