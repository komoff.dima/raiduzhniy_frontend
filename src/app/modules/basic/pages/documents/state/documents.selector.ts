import { AppStateInterface } from '@core/types/app-state.interface';
import { createSelector } from '@ngrx/store';

export const selectFeature = (state: AppStateInterface) => state.documents;

export const selectDocuments = createSelector(
  selectFeature,
  state => state.documents
);

export const selectDocumentsIsLoading = createSelector(
  selectFeature,
  state => state.documentsIsLoading
);

export const selectDocumentsSortStatus = createSelector(
  selectFeature,
  state => state.sortStatus
);

export const selectDocumentsFilters = createSelector(
  selectFeature,
  state => state.filters
);

export const selectTotalPages = createSelector(
  selectFeature,
  state => state.totalPages
);

export const selectCurrentPageNumber = createSelector(
  selectFeature,
  state => state.currentPageNumber
);
