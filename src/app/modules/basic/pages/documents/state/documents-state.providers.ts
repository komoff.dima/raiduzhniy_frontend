import { EnvironmentProviders, Provider } from '@angular/core';
import { provideEffects } from '@ngrx/effects';
import { provideState } from '@ngrx/store';
import { DocumentsService } from '@shared/services';
import { DocumentsEffects } from './documents.effects';
import { DocumentsFacade } from './documents.facade';
import { documentsReducer } from './documents.reducer';

export const DocumentsStateProviders: (Provider | EnvironmentProviders)[] = [
  provideEffects(DocumentsEffects),
  provideState({
    name: 'documents',
    reducer: documentsReducer,
  }),
  DocumentsService,
  DocumentsFacade,
];
