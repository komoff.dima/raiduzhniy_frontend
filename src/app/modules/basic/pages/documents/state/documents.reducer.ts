import { createReducer, on } from '@ngrx/store';
import { DOCUMENTS_INITIAL_SORTING_OBJECT } from '../documents.constant';
import { DocumentsStateInterface } from './documents-state.interface';
import * as DocumentsActions from './documents.actions';

export const initialState: DocumentsStateInterface = {
  currentPageNumber: 0,
  totalPages: undefined,
  documents: [],
  documentsIsLoading: false,
  documentsError: undefined,
  sortStatus: {
    orderBy: DOCUMENTS_INITIAL_SORTING_OBJECT.key,
    orderDirection: DOCUMENTS_INITIAL_SORTING_OBJECT.direction,
  },
  filters: {},
};

export const documentsReducer = createReducer<DocumentsStateInterface>(
  initialState,
  on(DocumentsActions.sortDocuments, (state, action) => ({
    ...state,
    documentsIsLoading: true,
    sortStatus: action.sortStatus,
    totalPages: undefined,
  })),
  on(DocumentsActions.sortFilters, (state, action) => ({
    ...state,
    documentsIsLoading: true,
    filters: action.filters,
    totalPages: undefined,
  })),
  on(
    DocumentsActions.getDocuments,
    (state, { filters, sortStatus, pageNumber }) => ({
      ...state,
      documentsIsLoading: true,
      ...(sortStatus ? { sortStatus } : {}),
      ...(filters ? { filters } : {}),
      documents: pageNumber === 1 ? [] : state.documents,
    })
  ),
  on(DocumentsActions.getDocumentsSuccess, (state, action) => ({
    ...state,
    documentsIsLoading: false,
    documents: [...state.documents, ...action.documents.elements],
    totalPages: action.documents.totalPages,
    currentPageNumber: action.documents.pageNumber,
  })),
  on(DocumentsActions.getDocumentsFailure, (state, action) => ({
    ...state,
    documentsIsLoading: false,
    documentsError: action.error,
  })),
  on(DocumentsActions.resetState, () => initialState)
);
