import { inject, Injectable } from '@angular/core';
import { AppStateInterface } from '@core/types/app-state.interface';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { DocumentsService } from '@shared/services';
import { catchError, EMPTY, map, mergeMap, of, switchMap } from 'rxjs';
import * as DocumentsActions from './documents.actions';
import {
  selectCurrentPageNumber,
  selectDocumentsFilters,
  selectDocumentsSortStatus,
  selectTotalPages,
} from './documents.selector';

@Injectable()
export class DocumentsEffects {
  private documentsService = inject(DocumentsService);
  private actions$ = inject(Actions);
  private store: Store<AppStateInterface> = inject(Store);

  getDocuments$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(DocumentsActions.getDocuments),
      concatLatestFrom(() => [
        this.store.select(selectDocumentsSortStatus),
        this.store.select(selectDocumentsFilters),
      ]),
      switchMap(([{ pageNumber }, sortStatus, filters]) => {
        return this.documentsService
          .getDocuments(
            {
              ...sortStatus,
              pageNumber,
              pageSize: 20,
            },
            filters
          )
          .pipe(
            map(response =>
              DocumentsActions.getDocumentsSuccess({
                documents: response,
              })
            ),
            catchError(error =>
              of(
                DocumentsActions.getDocumentsFailure({
                  error: error.message,
                })
              )
            )
          );
      })
    );
  });

  sortDocuments$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(DocumentsActions.sortDocuments),
      switchMap(({ sortStatus }) => {
        return of(DocumentsActions.getDocuments({ sortStatus, pageNumber: 1 }));
      })
    );
  });

  filterDocuments$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(DocumentsActions.sortFilters),
      switchMap(({ filters }) => {
        return of(DocumentsActions.getDocuments({ filters, pageNumber: 1 }));
      })
    );
  });

  getNextDocumentsPage$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(DocumentsActions.getNextDocumentsPage),
      concatLatestFrom(() => [
        this.store.select(selectTotalPages),
        this.store.select(selectCurrentPageNumber),
      ]),
      mergeMap(([_, totalPages, currentPageNumber]) => {
        const nextPage = currentPageNumber + 1;

        if (!totalPages || nextPage <= totalPages) {
          return of(DocumentsActions.getDocuments({ pageNumber: nextPage }));
        }

        return EMPTY;
      })
    );
  });
}
