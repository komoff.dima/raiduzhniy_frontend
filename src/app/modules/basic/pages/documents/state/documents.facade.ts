import { Injectable } from '@angular/core';
import { AppStateInterface } from '@core/types/app-state.interface';
import { Store } from '@ngrx/store';
import { AdaptedFilters } from '@shared/components/filterable-content';
import { SortStatus } from '@shared/interfaces';
import * as DocumentsActions from './documents.actions';
import {
  selectDocuments,
  selectDocumentsIsLoading,
} from './documents.selector';

@Injectable()
export class DocumentsFacade {
  documentsIsLoading$ = this.store.select(selectDocumentsIsLoading);

  documents$ = this.store.select(selectDocuments);

  constructor(private store: Store<AppStateInterface>) {}

  dispatchSortDocuments(sortStatus?: SortStatus): void {
    this.store.dispatch(DocumentsActions.sortDocuments({ sortStatus }));
  }

  dispatchFilterDocuments(filters: AdaptedFilters): void {
    this.store.dispatch(DocumentsActions.sortFilters({ filters }));
  }

  dispatchGetNextDocumentsPage(): void {
    this.store.dispatch(DocumentsActions.getNextDocumentsPage());
  }

  dispatchResetState(): void {
    this.store.dispatch(DocumentsActions.resetState());
  }
}
