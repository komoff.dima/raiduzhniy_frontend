import { AdaptedFilters } from '@shared/components/filterable-content';
import { IDocument, SortStatus } from '@shared/interfaces';

export interface DocumentsStateInterface {
  currentPageNumber: number;
  totalPages: number;
  documents: IDocument[];
  documentsIsLoading: boolean;
  documentsError: string;
  sortStatus: SortStatus;
  filters: AdaptedFilters;
}
