import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatCardModule } from '@angular/material/card';
import { FilesComponent } from '@shared/components';
import { BASIC_DATE_FORMAT } from '@shared/constants';
import { IDocument } from '@shared/interfaces';

@Component({
  selector: 'rdn-document-card',
  standalone: true,
  imports: [CommonModule, FilesComponent, MatCardModule],
  templateUrl: './document-card.component.html',
  styleUrls: ['./document-card.component.scss'],
})
export class DocumentCardComponent {
  dateFormat = BASIC_DATE_FORMAT;

  @Input() document: IDocument;
}
