import { CommonModule } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatCardModule } from '@angular/material/card';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import {
  FilesComponent,
  FilterableContentComponent,
  LoaderComponent,
} from '@shared/components';
import { AdaptedFilters } from '@shared/components/filterable-content';
import { SortOption } from '@shared/components/filterable-content/components';
import { FILTERS_TOKEN } from '@shared/components/filterable-content/filtarable-content.token';
import { LoaderDirective } from '@shared/directives';
import { IDocument } from '@shared/interfaces';
import { DocumentTypesService } from '@shared/services';
import { filter, startWith, withLatestFrom } from 'rxjs';
import { ScrollableContainerService } from '../../services';
import { NewsCardComponent } from '../news/components/news-card/news-card.component';
import { DocumentCardComponent } from './components/document-card/document-card.component';
import { documentFiltersFactory } from './documents.constant';
import { DocumentsFacade } from './state';

@Component({
  selector: 'rdn-documents',
  standalone: true,
  imports: [
    CommonModule,
    FilterableContentComponent,
    LoaderComponent,
    NewsCardComponent,
    LoaderDirective,
    MatCardModule,
    FilesComponent,
    DocumentCardComponent,
  ],
  templateUrl: './documents.component.html',
  styleUrls: ['./documents.component.scss'],
  providers: [
    DocumentTypesService,
    {
      provide: FILTERS_TOKEN,
      useFactory: documentFiltersFactory,
      deps: [DocumentTypesService],
    },
  ],
})
@UntilDestroy()
export class DocumentsComponent implements OnInit, OnDestroy {
  isLoading$ = this.documentsFacade.documentsIsLoading$;

  documents$ = this.documentsFacade.documents$;

  constructor(
    private documentsFacade: DocumentsFacade,
    private scrollableContainerService: ScrollableContainerService
  ) {}

  ngOnInit() {
    this.scrollableContainerService.scrolled$
      .pipe(
        untilDestroyed(this),
        startWith(null),
        withLatestFrom(this.isLoading$),
        filter(([_, isLoading]) => !isLoading)
      )
      .subscribe(() => this.documentsFacade.dispatchGetNextDocumentsPage());
  }

  filterDocuments(selectedFilters: AdaptedFilters) {
    this.documentsFacade.dispatchFilterDocuments(selectedFilters);
  }

  sortDocuments({ key, direction }: SortOption) {
    this.documentsFacade.dispatchSortDocuments({
      orderBy: key,
      orderDirection: direction,
    });
  }

  trackByDocuments(index: number, document: IDocument): string {
    return document.id;
  }

  ngOnDestroy() {
    this.documentsFacade.dispatchResetState();
  }
}
