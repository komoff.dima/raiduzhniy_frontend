import { Route } from '@angular/router';
import { NewsComponent } from './news.component';
import { ReadNewsComponent } from './pages/read-news/read-news.component';

export const NEWS_ROUTES: Route[] = [
  {
    path: '',
    component: NewsComponent,
  },
  {
    path: ':newsId',
    component: ReadNewsComponent,
  },
];
