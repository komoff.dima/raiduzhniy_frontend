import { createAction, props } from '@ngrx/store';
import { News, TableData } from '@shared/interfaces';

const IDENTIFIER = 'News';

export const getNewsForRead = createAction(
  `[${IDENTIFIER}] Get News`,
  props<{ id: string }>()
);
export const getNewsForReadSuccess = createAction(
  `[${IDENTIFIER}] Get News Success`,
  props<{ news: News }>()
);

export const getNewsForReadFailure = createAction(
  `[${IDENTIFIER}] Get News Failure`,
  props<{ error: string }>()
);

export const getNewsElementsNextPage = createAction(
  `[${IDENTIFIER}] Get News Elements Next Page`
);

export const getNewsElementsNextPageCancel = createAction(
  `[${IDENTIFIER}] Get News Elements Next Page Cancel`
);

export const getNewsElementsSuccess = createAction(
  `[${IDENTIFIER}] Get News Elements Success`,
  props<{ response: TableData<News> }>()
);

export const getNewsElementsFailure = createAction(
  `[${IDENTIFIER}] Get News Elements Failure`,
  props<{ error: string }>()
);

export const resetNewsState = createAction(`[${IDENTIFIER}] Reset News State`);

export const clearNewsForRead = createAction(
  `[${IDENTIFIER}] Clear News For Read`
);
