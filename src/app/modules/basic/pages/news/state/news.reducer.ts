import { createReducer, on } from '@ngrx/store';
import { NewsStateInterface } from './news-state.interface';
import * as NewsActions from './news.actions';

export const initialState: NewsStateInterface = {
  paginatorState: {
    pageLoaded: 0,
    totalPages: undefined,
  },
  isLoadingNewsElements: false,
  newsElements: [],
  errorNewsElements: '',
  isLoadingNews: false,
  news: null,
  errorNews: '',
};

export const newsReducer = createReducer<NewsStateInterface>(
  initialState,
  on(NewsActions.getNewsForRead, state => ({
    ...state,
    isLoadingNews: true,
  })),
  on(NewsActions.getNewsForReadSuccess, (state, action) => ({
    ...state,
    isLoadingNews: false,
    news: action.news,
  })),
  on(NewsActions.getNewsForReadFailure, (state, action) => ({
    ...state,
    isLoadingNews: false,
    errorNews: action.error,
  })),
  on(NewsActions.getNewsElementsNextPage, state => ({
    ...state,
    isLoadingNewsElements: true,
  })),
  on(NewsActions.getNewsElementsSuccess, (state, action) => ({
    ...state,
    isLoadingNewsElements: false,
    newsElements: [...state.newsElements, ...action.response.elements],
    paginatorState: {
      ...state.paginatorState,
      pageLoaded: action.response.pageNumber,
      totalPages: action.response.totalPages,
    },
  })),
  on(NewsActions.getNewsElementsNextPageCancel, state => ({
    ...state,
    isLoadingNewsElements: false,
  })),
  on(NewsActions.getNewsElementsFailure, (state, action) => ({
    ...state,
    isLoadingNewsElements: false,
    errorNewsElements: action.error,
  })),
  on(NewsActions.resetNewsState, () => initialState),
  on(NewsActions.clearNewsForRead, state => ({ ...state, news: null }))
);
