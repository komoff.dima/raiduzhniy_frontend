import { Injectable } from '@angular/core';
import { AppStateInterface } from '@core/types/app-state.interface';
import { Store } from '@ngrx/store';
import {
  clearNewsForRead,
  getNewsElementsNextPage,
  getNewsForRead,
  resetNewsState,
} from './news.actions';
import {
  selectIsLoadingNews,
  selectIsLoadingNewsElements,
  selectNews,
  selectNewsElements,
  selectNewsElementsError,
} from './news.selector';

@Injectable()
export class NewsFacade {
  isLoadingNews$ = this.store.select(selectIsLoadingNews);
  news$ = this.store.select(selectNews);
  newsElements$ = this.store.select(selectNewsElements);
  isLoadingNewsElements$ = this.store.select(selectIsLoadingNewsElements);
  errorNewsElements$ = this.store.select(selectNewsElementsError);

  constructor(private store: Store<AppStateInterface>) {}

  dispatchGetNewsNextPage(): void {
    this.store.dispatch(getNewsElementsNextPage());
  }

  dispatchGetNewsForRead(id: string): void {
    this.store.dispatch(getNewsForRead({ id }));
  }

  dispatchResetState(): void {
    this.store.dispatch(resetNewsState());
  }

  dispatchClearNewsForRead(): void {
    this.store.dispatch(clearNewsForRead());
  }
}
