import { inject, Injectable } from '@angular/core';
import { AppStateInterface } from '@core/types/app-state.interface';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { OrderDirection } from '@shared/interfaces';
import { NewsService } from '@shared/services';
import { catchError, exhaustMap, map, mergeMap, of } from 'rxjs';
import * as NewsActions from './news.actions';
import { selectPaginatorState } from './news.selector';

const DEFAULT_QUERY_PARAMS = {
  orderDirection: OrderDirection.Desc,
  orderBy: 'createdAt',
  pageSize: 10,
};

@Injectable()
export class NewsEffects {
  private newsService = inject(NewsService);
  private actions$ = inject(Actions);
  private store: Store<AppStateInterface> = inject(Store);

  getNewsElementsNextPage$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(NewsActions.getNewsElementsNextPage),
      concatLatestFrom(() => this.store.select(selectPaginatorState)),
      exhaustMap(([_, paginatorState]) => {
        if (
          paginatorState.pageLoaded === 0 ||
          paginatorState.pageLoaded < paginatorState.totalPages
        ) {
          return this.newsService
            .getNewsElements({
              ...DEFAULT_QUERY_PARAMS,
              pageNumber: paginatorState.pageLoaded + 1,
            })
            .pipe(
              map(response => NewsActions.getNewsElementsSuccess({ response })),
              catchError(error =>
                of(NewsActions.getNewsElementsFailure({ error: error.message }))
              )
            );
        }

        return of(NewsActions.getNewsElementsNextPageCancel());
      })
    );
  });

  getNewsForRead$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(NewsActions.getNewsForRead),
      mergeMap(({ id }) =>
        this.newsService.getNews(id).pipe(
          map(news => NewsActions.getNewsForReadSuccess({ news })),
          catchError(error =>
            of(NewsActions.getNewsForReadFailure({ error: error.message }))
          )
        )
      )
    );
  });
}
