import { News } from '@shared/interfaces';

export interface NewsPaginatorState {
  pageLoaded: number;
  totalPages: number;
}

export interface NewsStateInterface {
  paginatorState: NewsPaginatorState;
  isLoadingNewsElements: boolean;
  newsElements: News[];
  errorNewsElements: string;
  isLoadingNews: boolean;
  news: News | null;
  errorNews: string;
}
