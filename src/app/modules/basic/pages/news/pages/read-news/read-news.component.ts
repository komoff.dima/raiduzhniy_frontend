import { CommonModule } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { WrapperComponent } from '@shared/components';
import { NewsCardComponent } from '../../components/news-card/news-card.component';
import { NewsFacade } from '../../state';

@Component({
  selector: 'rdn-read-news',
  standalone: true,
  imports: [CommonModule, NewsCardComponent, WrapperComponent],
  templateUrl: './read-news.component.html',
  styleUrls: ['./read-news.component.scss'],
})
export class ReadNewsComponent implements OnInit, OnDestroy {
  isLoading$ = this.newsFacade.isLoadingNews$;

  news$ = this.newsFacade.news$;

  private readonly newsId: string = this.route.snapshot.params['newsId'];
  constructor(
    private route: ActivatedRoute,
    private newsFacade: NewsFacade
  ) {}

  ngOnInit() {
    this.newsFacade.dispatchGetNewsForRead(this.newsId);
  }

  ngOnDestroy() {
    this.newsFacade.dispatchClearNewsForRead();
  }
}
