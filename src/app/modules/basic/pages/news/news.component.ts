import { CommonModule, NgOptimizedImage } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { MatCardModule } from '@angular/material/card';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { UpdatesDependentComponent } from '@shared/abstract-classes';
import { LoaderComponent, WrapperComponent } from '@shared/components';
import { LoaderDirective } from '@shared/directives';
import { News } from '@shared/interfaces';
import { ScrollableContainerService } from '../../services';
import { NewsCardComponent } from './components/news-card/news-card.component';
import { NewsFacade } from './state';

@Component({
  selector: 'rdn-news',
  standalone: true,
  imports: [
    CommonModule,
    MatCardModule,
    WrapperComponent,
    NgOptimizedImage,
    LoaderDirective,
    NewsCardComponent,
    LoaderComponent,
  ],
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss'],
})
@UntilDestroy()
export class NewsComponent extends UpdatesDependentComponent implements OnInit {
  news$ = this.newsFacade.newsElements$;
  isLoading$ = this.newsFacade.isLoadingNewsElements$;
  constructor(
    private scrollableContainerService: ScrollableContainerService,
    private newsFacade: NewsFacade
  ) {
    super('news');
  }

  override ngOnInit() {
    super.ngOnInit();
    this.scrollableContainerService.scrolled$
      .pipe(untilDestroyed(this))
      .subscribe(() => this.newsFacade.dispatchGetNewsNextPage());
  }

  updateState(): void {
    this.newsFacade.dispatchResetState();
    this.newsFacade.dispatchGetNewsNextPage();
  }

  trackByFn(index: number, news: News): string {
    return news.id;
  }
}
