import { CommonModule, NgOptimizedImage } from '@angular/common';
import {
  AfterViewInit,
  Component,
  ElementRef,
  Input,
  ViewChild,
} from '@angular/core';
import { MatCardModule } from '@angular/material/card';
import { RouterLink } from '@angular/router';
import { UntilDestroy } from '@ngneat/until-destroy';
import { News } from '@shared/interfaces';

@Component({
  selector: 'rdn-news-card',
  standalone: true,
  imports: [CommonModule, MatCardModule, NgOptimizedImage, RouterLink],
  templateUrl: './news-card.component.html',
  styleUrls: ['./news-card.component.scss'],
})
@UntilDestroy()
export class NewsCardComponent implements AfterViewInit {
  readonly maxTextContainerHeight = 500;

  needCoverText = false;

  @Input() news: News;

  @Input() linkMode = true;

  @ViewChild('htmlDivElement')
  htmlDivElementElementRef: ElementRef<HTMLDivElement>;

  ngAfterViewInit() {
    if (this.linkMode) {
      setTimeout(() => this.determineNeedCoverText(), 0);
    }
  }

  private determineNeedCoverText() {
    this.needCoverText =
      this.htmlDivElementElementRef.nativeElement.offsetHeight >
      this.maxTextContainerHeight;
  }
}
