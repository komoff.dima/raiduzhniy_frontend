import { CommonModule, DOCUMENT, NgOptimizedImage } from '@angular/common';
import { AfterViewInit, Component, Inject, ViewChild } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import {
  MatSidenav,
  MatSidenavContent,
  MatSidenavModule,
} from '@angular/material/sidenav';
import { MatTabsModule } from '@angular/material/tabs';
import { MatToolbarModule } from '@angular/material/toolbar';
import {
  NavigationEnd,
  Route,
  Router,
  RouterLink,
  RouterLinkActive,
  RouterOutlet,
} from '@angular/router';
import { BreakpointsService } from '@core/services';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { UserMenuComponent } from '@shared/components';
import { LoginDialogComponent } from '@shared/components/dialogs';
import { COMMON_DIALOG_CONFIG } from '@shared/constants';
import { ForRolesDirective } from '@shared/directives';
import { Breakpoints } from '@shared/enums';
import { LogoWithoutLabelIconComponent } from '@shared/svg-icons';
import { MainLogoComponent } from '@shared/svg-icons/main-logo.icon';
import { AuthFacade } from '@state/auth';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { IInfiniteScrollEvent } from 'ngx-infinite-scroll/models';
import { filter, fromEvent, map, Observable } from 'rxjs';
import { COMMON_ROUTES } from './basic.routes';
import { ScrollableContainerService } from './services';

@Component({
  selector: 'rdn-basic',
  standalone: true,
  imports: [
    CommonModule,
    RouterOutlet,
    MatToolbarModule,
    MatTabsModule,
    RouterLink,
    RouterLinkActive,
    MatMenuModule,
    MatButtonModule,
    MatIconModule,
    NgOptimizedImage,
    MatDialogModule,
    ForRolesDirective,
    MainLogoComponent,
    UserMenuComponent,
    MatSidenavModule,
    MatListModule,
    LogoWithoutLabelIconComponent,
    InfiniteScrollModule,
  ],
  templateUrl: './basic.component.html',
  styleUrls: ['./basic.component.scss'],
})
@UntilDestroy()
export class BasicComponent implements AfterViewInit {
  user$ = this.authFacade.user$;

  isMobileView$ = this.breakpointsService
    .observeBreakpoint(Breakpoints.LG)
    .pipe(map(matches => !matches));

  showScrollTopButton$: Observable<boolean>;

  @ViewChild(MatSidenav) sidenav: MatSidenav;

  @ViewChild(MatSidenavContent) scrollableContainer: MatSidenavContent;

  constructor(
    private dialog: MatDialog,
    private authFacade: AuthFacade,
    private breakpointsService: BreakpointsService,
    private router: Router,
    private scrollableContainerService: ScrollableContainerService,
    @Inject(DOCUMENT) private document: Document
  ) {}

  commonRoutes: Route[] = COMMON_ROUTES;

  ngAfterViewInit() {
    this.router.events
      .pipe(
        untilDestroyed(this),
        filter(event => event instanceof NavigationEnd)
      )
      .subscribe(() => this.sidenav.close());

    this.showScrollTopButton$ = fromEvent(
      this.scrollableContainer.getElementRef().nativeElement,
      'scroll'
    ).pipe(
      map(event => {
        const target = event.target as HTMLElement;

        return target.scrollTop > window.innerHeight;
      })
    );
  }

  openLoginDialog() {
    this.dialog.open(LoginDialogComponent, {
      ...COMMON_DIALOG_CONFIG,
    });
  }

  trackByFn(index: number): number {
    return index;
  }

  scrolled(event: IInfiniteScrollEvent) {
    this.scrollableContainerService.onScroll(event);
  }

  scrollTop(): void {
    this.scrollableContainer.getElementRef().nativeElement.scrollTo({
      top: 0,
      behavior: 'smooth',
    });
  }
}
