import { Injectable } from '@angular/core';
import { IInfiniteScrollEvent } from 'ngx-infinite-scroll/models';
import { Subject } from 'rxjs';

@Injectable()
export class ScrollableContainerService {
  private readonly _scrolled$ = new Subject<IInfiniteScrollEvent>();

  readonly scrolled$ = this._scrolled$.asObservable();

  onScroll(event: IInfiniteScrollEvent): void {
    this._scrolled$.next(event);
  }
}
