import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { SseService } from '@core/services/sse.service';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { UpdatesFacade } from '@state/updates';

@Component({
  selector: 'rdn-root',
  standalone: true,
  imports: [CommonModule, RouterOutlet],
  styleUrls: ['./app.component.scss'],
  template: ` <router-outlet></router-outlet> `,
})
@UntilDestroy()
export class AppComponent {
  constructor(
    private sseService: SseService,
    private updatesFacade: UpdatesFacade
  ) {
    this.observeSseEvents();
  }

  private observeSseEvents(): void {
    this.sseService.events$.pipe(untilDestroyed(this)).subscribe(event => {
      this.updatesFacade.dispatchAddUpdate(event);
    });
  }
}
