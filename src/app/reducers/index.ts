import { isDevMode } from '@angular/core';
import { ActionReducerMap, MetaReducer } from '@ngrx/store';
import { authReducer } from '@state/auth/auth.reducer';
import { updatesReducer } from '@state/updates/updates.reducer';

export interface State {}

export const reducers: ActionReducerMap<State> = {
  user: authReducer,
  updates: updatesReducer,
};

export const metaReducers: MetaReducer<State>[] = isDevMode() ? [] : [];
