import { registerLocaleData } from '@angular/common';
import { provideHttpClient, withInterceptors } from '@angular/common/http';
import ukLocale from '@angular/common/locales/uk';
import {
  APP_INITIALIZER,
  ApplicationConfig,
  importProvidersFrom,
  LOCALE_ID,
} from '@angular/core';
import { ErrorStateMatcher, MatNativeDateModule } from '@angular/material/core';
import { MatPaginatorIntl } from '@angular/material/paginator';
import { provideAnimations } from '@angular/platform-browser/animations';
import { provideRouter } from '@angular/router';
import { CustomPaginatorSettings, MyErrorStateMatcher } from '@core/classes';
import { UserInitializer } from '@core/initializers/user.initializer';
import { AuthInterceptor } from '@core/interceptors';
import { provideEffects } from '@ngrx/effects';
import { provideStore } from '@ngrx/store';
import { provideStoreDevtools } from '@ngrx/store-devtools';
import { AuthEffects } from '@state/auth/auth.effects';
import { environment } from '../environments/environment';

import { routes } from './app.routes';
import { metaReducers, reducers } from './reducers';

registerLocaleData(ukLocale);

export const appConfig: ApplicationConfig = {
  providers: [
    provideRouter(routes),
    provideAnimations(),
    provideHttpClient(withInterceptors([AuthInterceptor])),
    provideStore(reducers, { metaReducers }),
    provideEffects(AuthEffects),
    importProvidersFrom(MatNativeDateModule),
    provideStoreDevtools({
      maxAge: 25, // Retains last 25 states
      logOnly: !environment.production, // Restrict extension to log-only mode
      autoPause: true, // Pauses recording actions and state changes when the extension window is not open
      trace: false, //  If set to true, will include stack trace for every dispatched action, so you can see it in trace tab jumping directly to that part of code
      traceLimit: 75, // maximum stack trace frames to be stored (in case trace option was provided as true)
    }),
    { provide: ErrorStateMatcher, useClass: MyErrorStateMatcher },
    {
      provide: MatPaginatorIntl,
      useClass: CustomPaginatorSettings,
    },
    { provide: LOCALE_ID, useValue: 'uk' },
    {
      provide: APP_INITIALIZER,
      useFactory: UserInitializer,
      multi: true,
    },
  ],
};
