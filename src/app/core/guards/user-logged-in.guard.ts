import { inject } from '@angular/core';
import { CanActivateFn, Router } from '@angular/router';
import { AuthFacade } from '@state/auth';
import { map, Observable } from 'rxjs';

export const userLoggedInGuard: CanActivateFn = (): Observable<boolean> => {
  const authFacade = inject(AuthFacade);
  const router = inject(Router);

  return authFacade.user$.pipe(
    map(user => {
      const allowed = !!user;

      if (!allowed) {
        router.navigate(['/404']);
      }

      return allowed;
    })
  );
};
