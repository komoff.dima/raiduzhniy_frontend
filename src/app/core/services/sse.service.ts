import { Injectable } from '@angular/core';
import { Update } from '@shared/interfaces';
import { Observable, Subject } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class SseService {
  private readonly baseUrl = environment.apiUrl;

  private _events$ = new Subject<Update>();

  private onMessageFunction = (event: MessageEvent<string>) => {
    this._events$.next(JSON.parse(event.data));
  };

  private eventSource: EventSource = new EventSource(
    `${this.baseUrl}/sse/updates`
  );

  unsubscribeFromEvents(): void {
    this.eventSource.onmessage = null;
  }

  get events$(): Observable<Update> {
    if (!this.eventSource.onmessage) {
      this.eventSource.onmessage = this.onMessageFunction;
    }

    return this._events$;
  }
}
