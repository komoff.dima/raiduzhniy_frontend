import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';
import { DOCUMENT } from '@angular/common';
import { Inject, Injectable } from '@angular/core';
import { Breakpoints } from '@shared/enums';
import { distinctUntilChanged, map, Observable } from 'rxjs';

type BreakpointValues = {
  [key in Breakpoints]: string;
};

const getBreakpointCondition = (breakpoint: string): string =>
  `(min-width: ${breakpoint})`;
@Injectable({ providedIn: 'root' })
export class BreakpointsService {
  private _breakpointValues: BreakpointValues;

  constructor(
    private breakpointObserver: BreakpointObserver,
    @Inject(DOCUMENT) private document: Document
  ) {
    this.readBreakpointsFromDocument();
  }

  observeBreakpoint(breakpoint: Breakpoints): Observable<boolean> {
    return this.observeBreakpoints$.pipe(
      map(
        state =>
          state.breakpoints[
            getBreakpointCondition(this.breakpointValues[breakpoint])
          ]
      ),
      distinctUntilChanged()
    );
  }

  get breakpointValues(): BreakpointValues {
    return this._breakpointValues || ({} as BreakpointValues);
  }

  readBreakpointsFromDocument(): void {
    this._breakpointValues = Object.values(Breakpoints).reduce(
      (obj, breakpoint) => {
        obj[breakpoint] = getComputedStyle(this.document.documentElement)
          .getPropertyValue(breakpoint)
          .trim();

        return obj;
      },
      {} as BreakpointValues
    );
  }

  private get observeBreakpoints$(): Observable<BreakpointState> {
    return this.breakpointObserver.observe(
      Object.values(Breakpoints).map(breakpoint =>
        getBreakpointCondition(this.breakpointValues[breakpoint])
      )
    );
  }
}
