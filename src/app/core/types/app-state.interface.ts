import { AboutUsStateInterface } from '@state/about-us/about-us-state.interface';
import { AuthStateInterface } from '@state/auth/auth-state.interface';
import { ContactsStateInterface } from '@state/contacts/contacts-state.interface';
import { AdminDocumentTypesStateInterface } from '../../modules/admin/pages/admin-documents/pages/document-types/state/admin-document-types-state.interface';
import { AdminDocumentsStateInterface } from '../../modules/admin/pages/admin-documents/state/admin-documents-state.interface';
import { AdminNewsStateInterface } from '../../modules/admin/pages/admin-news/state/admin-news-state.interface';
import { UpdatesStateInterface } from '@state/updates/updates-state.interface';
import { DocumentsStateInterface } from '../../modules/basic/pages/documents/state/documents-state.interface';
import { NewsStateInterface } from '../../modules/basic/pages/news/state/news-state.interface';

export interface AppStateInterface {
  user: AuthStateInterface;
  aboutUs: AboutUsStateInterface;
  contacts: ContactsStateInterface;
  adminNews: AdminNewsStateInterface;
  news: NewsStateInterface;
  updates: UpdatesStateInterface;
  adminDocumentTypes: AdminDocumentTypesStateInterface;
  adminDocuments: AdminDocumentsStateInterface;
  documents: DocumentsStateInterface;
}
